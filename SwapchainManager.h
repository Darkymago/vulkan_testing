#pragma once

#include <stdexcept>
#include <vector>

#include <algorithm>

#include "Common.h"

#include "HelperStructs.h"
#include "ImageHelpers.h"

#include "DeviceManager.h"

class SwapchainManager
{
public:
  //struct SwapchainWrapper
  //{
  //  DeviceManager const* deviceMgmt;
  //  DeviceManager const* deviceMgmt;

  //  size_t iBeginImages;
  //  size_t iCountImages;

  //  VkFormat m_imageFormat;
  //  VkExtent2D m_extent;

  //  VkSwapchainKHR m_swapchain;
  //}

  VkSwapchainKHR m_swapchain;

  VkFormat m_imageFormat;
  VkExtent2D m_extent;

  DeviceManager const* m_pDeviceMgmt;

  std::vector<VkImage> m_swapchainImages;
  std::vector<VkImageView> m_swapchainImageViews;

  //std::vector<SwapchainWrapper> m_swapchainWrappers;

public:
  void init(GLFWwindow* window, DeviceManager const& deviceManager)
  {
    m_pDeviceMgmt = &deviceManager;

    createSwapchain(window);
    createImageViews();
  }
  //void addSwapchain(GLFWwindow* window, DeviceManager const& deviceManager)
  //{
  //  m_pDeviceMgmt = &deviceManager;

  //  createSwapchain(window);
  //  createImageViews();
  //}

  void cleanup()
  {
    for (auto imageView : m_swapchainImageViews)
    {
      vkDestroyImageView(m_pDeviceMgmt->m_deviceSet.device, imageView, nullptr);
    }

    vkDestroySwapchainKHR(m_pDeviceMgmt->m_deviceSet.device, m_swapchain, nullptr);
  }

private:
  void createSwapchain(GLFWwindow* window)
  {
    SwapChainSupportDetails swapChainSupport = SwapChainSupportDetails::QuerySwapChainSupport(m_pDeviceMgmt->m_deviceSet.physicalDevice, m_pDeviceMgmt->m_deviceSet.surface);

    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
    VkExtent2D extent = chooseSwapExtent(window, swapChainSupport.capabilities);

    // Image# in swap buffer
    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
    {
      imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = m_pDeviceMgmt->m_deviceSet.surface;

    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    // Specify how to handle swapchain images across multiple queues
    QueueFamilyIndices indices = QueueFamilyIndices::GetQueueFamilies(m_pDeviceMgmt->m_deviceSet);
    uint32_t queueFamilyIndices[] = {(uint32_t) indices.graphicsFamily.value(), (uint32_t) indices.presentFamily.value()};

    if (indices.graphicsFamily != indices.presentFamily)
    {
      createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
      createInfo.queueFamilyIndexCount = 2;
      createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }
    else
    {
      createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
      createInfo.queueFamilyIndexCount = 0; // Optional
      createInfo.pQueueFamilyIndices = nullptr; // Optional
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;

    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;

    createInfo.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(m_pDeviceMgmt->m_deviceSet.device, &createInfo, nullptr, &m_swapchain) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create swap chain!");
    }

    vkGetSwapchainImagesKHR(m_pDeviceMgmt->m_deviceSet.device, m_swapchain, &imageCount, nullptr);
    m_swapchainImages.resize(imageCount);
    vkGetSwapchainImagesKHR(m_pDeviceMgmt->m_deviceSet.device, m_swapchain, &imageCount, m_swapchainImages.data());

    m_imageFormat = surfaceFormat.format;
    m_extent = extent;
  }

  void createImageViews()
  {
    m_swapchainImageViews.resize(m_swapchainImages.size());

    for (size_t i = 0; i < m_swapchainImages.size(); ++i)
    {
      m_swapchainImageViews[i] = Images::CreateImageView(m_pDeviceMgmt->m_deviceSet, m_swapchainImages[i], m_imageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
    }
  }

  VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
  {
    if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
    {
      return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }

    for (const auto& availableFormat : availableFormats)
    {
      if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
      {
        return availableFormat;
      }
    }

    return availableFormats[0];
  }

  VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
  {
    VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

    for (const auto& availablePresentMode : availablePresentModes)
    {
      switch (availablePresentMode)
      {
        case VK_PRESENT_MODE_MAILBOX_KHR:
          return availablePresentMode;
          break;
        case VK_PRESENT_MODE_IMMEDIATE_KHR:
          bestMode = availablePresentMode;
        case VK_PRESENT_MODE_FIFO_KHR:
        case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
        case VK_PRESENT_MODE_MAX_ENUM_KHR:
        case VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR:
        case VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR:
        //case VK_PRESENT_MODE_RANGE_SIZE_KHR:
          break;
      }
    }

    return bestMode;
  }

  VkExtent2D chooseSwapExtent(GLFWwindow* window, const VkSurfaceCapabilitiesKHR& capabilities)
  {
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
      return capabilities.currentExtent;
    }
    else
    {
      int width, height;
      glfwGetWindowSize(window, &width, &height);

      VkExtent2D actualExtent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

      actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
      actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

      return actualExtent;
    }
  }
};
