#pragma once

#include "../ShaderHelpers.h"
#include "../ObjLoader.h"

#include "../PipelineManager.h"

#include <glm/glm.hpp>

struct CelestialBodiesShader : public Shaders::ShaderPipeAbstract<CelestialBodiesShader>
{
  const std::string VERTEX_SHADER_PATH = "shaders/celestialBodies.vert.spv";
  const std::string FRAGMENT_SHADER_PATH = "shaders/celestialBodies.frag.spv";

  static std::vector<VkVertexInputAttributeDescription> const Attributes;
  static std::vector<VkVertexInputBindingDescription> const Bindings;
  static std::vector<VkDescriptorSetLayoutBinding> const LayoutBindings;

  static std::vector<VkDescriptorBufferInfo> UniformInfos;
  static std::vector<VkDescriptorImageInfo> TextureInfos;

  VkCommandPool m_commandPool;

  std::vector<glm::vec4> m_vertices;
  std::vector<uint32_t> m_indices;

  std::vector<glm::vec4> m_colors;
  std::vector<glm::mat4> m_transforms;

  typedef std::array<uint32_t, 2> DrawPart;
  struct ModelOffsets
  {
    uint32_t v_start, v_count;
    std::vector<DrawPart> parts;
  };
  std::vector<ModelOffsets> m_models;
  struct DrawInstance
  {
    size_t index;
    PipelineManager::PIPELINE_TYPE pipeType;
    size_t modelIdx;
    uint32_t offset, count;
    bool bActive;
  };
  std::vector<DrawInstance> m_draws;

  struct
  {
    Objects::VPMatrices vp;
    glm::vec4 clipPlane = {0.0, 0.0, 1.0, 0.0};
  } uniforms;
  Objects::VPMatrices& vp = uniforms.vp;

  bool bForms = false;
  bool bUpdateVertexBuffers = false;

  struct
  {
    BufferHandle uniforms;
    struct
    {
      BufferHandle colors;
      BufferHandle transforms;
    } instances;
    struct
    {
      BufferHandle vertices;
      BufferHandle indices;
    } elements;
  } buffers;

  void init(DeviceManager const& deviceManager, BufferManager& bufferManager, VkCommandPool commandPool)
  {
    ShaderPipeAbstract::init(deviceManager, bufferManager);

    m_commandPool = commandPool;

    reload(false);

    createUniformBuffer();
    setUniformBuffer(buffers.uniforms);
  }

  const size_t addModel(const std::vector<glm::vec4>& vertices, const std::vector<uint32_t>& indices, const std::vector<DrawPart>& parts = {})
  {
    uint32_t i_start = m_indices.size();
    uint32_t i_count = indices.size();
    ModelOffsets offsets = {
      (uint32_t) m_vertices.size(),
      (uint32_t) vertices.size(),
      {}
    };
    if (parts.empty())
      offsets.parts.push_back({i_start, i_count});

    for (auto& part : parts)
      offsets.parts.push_back({part[0] + i_start, part[1]});

    const auto& firstVertIt = m_vertices.end();

    // insert vertices
    m_vertices.reserve(offsets.v_start + offsets.v_count);
    m_vertices.insert(m_vertices.end(), vertices.begin(), vertices.end());

    // insert indices
    m_indices.reserve(i_start + i_count);
    m_indices.insert(m_indices.end(), indices.begin(), indices.end());
    std::for_each(m_indices.begin()+i_start, m_indices.end(), [&offsets](uint32_t& u){u += offsets.v_start;});


    const auto modelIdx = m_models.size();
    m_models.push_back(offsets);
    return modelIdx;
  }
  const size_t addDrawInstances(const size_t& modelIdx, const std::vector<glm::vec4>& colors, const std::vector<glm::mat4>& transforms, PipelineManager::PIPELINE_TYPE pipelineType = PipelineManager::PIPELINE_TYPE::TRIANGLES, bool bActive = true)
  {
    ModelOffsets& offsets = m_models[modelIdx];
    if (colors.size() != transforms.size())
      throw std::runtime_error("Not same amount of colors and transforms");

    auto idx = m_draws.size();
    DrawInstance instances = {
      idx,
      pipelineType,
      modelIdx,
      (uint32_t) m_colors.size(),
      (uint32_t) colors.size(),
      bActive
    };
    // insert instance objects (color, transform)
    m_colors.reserve(instances.offset + instances.count);
    m_transforms.reserve(instances.offset + instances.count);
    m_colors.insert(m_colors.end(), colors.begin(), colors.end());
    m_transforms.insert(m_transforms.end(), transforms.begin(), transforms.end());

    m_draws.push_back(instances);
    return idx;
  }
  void updateInstance(DrawInstance& instances, const std::vector<glm::vec4>& colors, const std::vector<glm::mat4>& transforms)
  {
   if (instances.count == colors.size() and instances.count == transforms.size())
    {
      memcpy(m_colors.data()+instances.offset, colors.data(), sizeof(colors[0])*colors.size());
      memcpy(m_transforms.data()+instances.offset, transforms.data(), sizeof(transforms[0])*transforms.size());
    }
  }

  void setShow(size_t drawIdx, bool bShow)
  {
    m_draws[drawIdx].bActive = bShow;
  }

  DrawInstance& getDraw(const size_t& drawIdx)
  {
    return m_draws[drawIdx];
  }

  void draw(VkCommandBuffer commandBuffer, PipelineManager const& pipelineMgmt)
  {
    VkBuffer vertexBuffers[] = {
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.elements.vertices),
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.instances.colors),
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.instances.transforms)
    };
    VkDeviceSize buffer_offsets[] = {0, 0, 0};

    // Constant Bindings
    vkCmdBindVertexBuffers(commandBuffer, 0, 3, vertexBuffers, buffer_offsets);
    vkCmdBindIndexBuffer(commandBuffer, ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.elements.indices), 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.m_pipelineLayout, 0, 1, &static_cast<ShaderPipeBase*>(this)->m_descriptorSet, 0, nullptr);

    // Drawing
    for (auto& instances : m_draws)
    {
      if (instances.bActive)
      {
        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getPipeline(instances.pipeType));
        for (auto& part : m_models[instances.modelIdx].parts)
        {
          vkCmdDrawIndexed(
              commandBuffer,
              part[1],
              instances.count,
              part[0],
              0,
              instances.offset
              );
        }
      }
    }
  }
  void setUpdateBufferSwitch(bool b){ bUpdateVertexBuffers = b; }
  void update()
  {
    ShaderPipeBase::m_pBufferMgmt->updateBuffer(buffers.uniforms, sizeof(uniforms), &uniforms);
    if (bUpdateVertexBuffers)
    {
      // Only update instance part, per vertex should not be touched as of yet
      ShaderPipeBase::m_pBufferMgmt->updateBuffer(
          buffers.instances.colors,
          sizeof(m_colors[0])*m_colors.size(),
          (void*) m_colors.data(),
          m_commandPool
      );
      ShaderPipeBase::m_pBufferMgmt->updateBuffer(
          buffers.instances.transforms,
          sizeof(m_transforms[0])*m_transforms.size(),
          (void*) m_transforms.data(),
          m_commandPool
      );
    }
  }
  void reload(bool bClean=true)
  {
    if (bClean)
      cleanup();

    loadMinimalGraphicsShaders(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
    createDescriptorSetLayout();
  }

  void setBuffers(VkCommandPool commandPool)
  {
    // Vertex Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_vertices[0])*m_vertices.size(),
        (void*) m_vertices.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.elements.vertices
    );
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_indices[0])*m_indices.size(),
        (void*) m_indices.data(),
        commandPool,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        buffers.elements.indices
    );
    // Color Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_colors[0])*m_colors.size(),
        (void*) m_colors.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.instances.colors
    );
    // Translations Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_transforms[0])*m_transforms.size(),
        (void*) m_transforms.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.instances.transforms
    );
  }
  void createUniformBuffer()
  {
    m_pBufferMgmt->createUniformBuffer(sizeof(uniforms), buffers.uniforms);
    setUniformBuffer(buffers.uniforms);
  }
  void cleanBuffers()
  {
    m_pBufferMgmt->cleanBuffer(buffers.elements.vertices);
    m_pBufferMgmt->cleanBuffer(buffers.elements.indices);
    m_pBufferMgmt->cleanBuffer(buffers.instances.colors);
    m_pBufferMgmt->cleanBuffer(buffers.instances.transforms);
    m_pBufferMgmt->cleanBuffer(buffers.uniforms);
  }
  void clear(VkDevice device)
  {
    m_vertices.clear();
    m_indices.clear();
    m_colors.clear();
    m_transforms.clear();
  }
};
const std::vector<VkVertexInputAttributeDescription> CelestialBodiesShader::Attributes = {
  // { location, binding, VkFormat, offset }
  {0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0}, // Vertex
  {1, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 0}, // Color
  {2, 2, VK_FORMAT_R32G32B32A32_SFLOAT, 0},  // Model
  {3, 2, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(glm::vec4)},  // Model
  {4, 2, VK_FORMAT_R32G32B32A32_SFLOAT, 2*sizeof(glm::vec4)},  // Model
  {5, 2, VK_FORMAT_R32G32B32A32_SFLOAT, 3*sizeof(glm::vec4)},  // Model
};
const std::vector<VkVertexInputBindingDescription> CelestialBodiesShader::Bindings = {
  // { binding, size, VkInputRate }
  {0, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_VERTEX},   // Vertex
  {1, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_INSTANCE}, // Color
  {2, sizeof(glm::mat4), VK_VERTEX_INPUT_RATE_INSTANCE}, // Model
};
const std::vector<VkDescriptorSetLayoutBinding> CelestialBodiesShader::LayoutBindings = {
  // { binding, VkDescriptorType, count, VkShaderStageFlags, pImmutableSamplers }
  {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT, nullptr}
};
std::vector<VkDescriptorBufferInfo> CelestialBodiesShader::UniformInfos = {
  // { VkHandle (Buffer to set after creation), offset, size }
  {VK_NULL_HANDLE, 0, sizeof(CelestialBodiesShader::uniforms)},
};
std::vector<VkDescriptorImageInfo> CelestialBodiesShader::TextureInfos = {
  // { VkHandle (Buffer to set after creation), offset, size }
};
