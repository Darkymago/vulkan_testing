#version 450
#extension GL_ARB_separate_shader_objects : enable

const float M_PI = 3.14159265358979323846; // pi
const float M_PI_2 = 1.57079632679489661923; // pi/2
const vec4 poleColor = vec4(1.0, 1.0, 1.0, 1.0);

layout(set = 0, binding = 0) uniform UniformBufferObject
{
  mat4 view;
  mat4 proj;
  vec4 clipPlane;
} ubo;

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in mat4 inModel;

layout(location = 0) out vec4 fragColor;

out gl_PerVertex
{
  vec4 gl_Position;
  float[] gl_ClipDistance;
};

void main()
{
  gl_Position =  inModel * inPosition;
  fragColor = inColor;

  gl_ClipDistance[0] = dot(ubo.clipPlane, gl_Position);
  gl_Position = ubo.proj * ubo.view* gl_Position;
}
