#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject
{
  mat4 view;
  mat4 proj;
} ubo;

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in mat4 inModel;

layout(location = 0) out vec4 fragColor;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
};

void main()
{
  gl_Position = ubo.proj * ubo.view * inModel * inPosition;
  fragColor = inColor;
}
