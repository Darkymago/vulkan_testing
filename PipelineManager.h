#pragma once

#include <vector>
#include <functional>

#include "Common.h"

#include "ShaderHelpers.h"

#include "DeviceManager.h"
#include "SwapchainManager.h"

class PipelineManager
{
public:
  enum PIPELINE_TYPE {
    POINTS = 0,
    LINES,
    LINE_STRIPS,
    TRIANGLES,
    TRIANGLE_STRIPS,
    TRIANGLE_STRIPS_TRANSPARENT,
    TRIANGLE_FANS,
    PIPELINE_TYPE_NB
  };

  std::array<VkPipeline, PIPELINE_TYPE_NB> m_graphicsPipelines;

public:
  VkPipelineLayout m_pipelineLayout;
  PIPELINE_TYPE m_pipelineType = PIPELINE_TYPE::TRIANGLES;

  DeviceManager const* m_pDeviceMgmt;
  SwapchainManager const* m_pSwapchainMgmt;

public:
  void init(DeviceManager const& deviceManager, SwapchainManager const& swapchainManager)
  {
    m_pDeviceMgmt = &deviceManager;
    m_pSwapchainMgmt = &swapchainManager;
  }

  void setCurrentPipeline(PIPELINE_TYPE type)
  {
    m_pipelineType = type;
  }
  VkPipeline const& getCurrentPipeline() const
  {
    return *(m_graphicsPipelines.data() + m_pipelineType);
  }
  VkPipeline const& getPipeline(PIPELINE_TYPE type) const
  {
    return *(m_graphicsPipelines.data() + type);
  }

  void swapCurrentPipeline()
  {
    m_pipelineType = static_cast<PIPELINE_TYPE>( (static_cast<int>(m_pipelineType) + 1) % static_cast<int>(PIPELINE_TYPE::PIPELINE_TYPE_NB));
  }

  void createGraphicsPipeline(Shaders::ShaderPipeBase& shader, VkRenderPass renderPass, const VkPolygonMode& polygonMode = VK_POLYGON_MODE_FILL, const VkFrontFace& rasterFrontFace = VkFrontFace::VK_FRONT_FACE_COUNTER_CLOCKWISE)//, VkDescriptorSetLayout descriptorSetLayout)
  {
    auto& shaderStages = shader.m_modulesManager.getStagesInfos();

    // Vertex Input Fixed Function
    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    auto& bindingDescriptions = shader.GetBindings();
    auto& attributeDescriptions = shader.GetAttributes();

    vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
    vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    // Input Assembly State Fixed Function
    std::array<VkPipelineInputAssemblyStateCreateInfo, PIPELINE_TYPE::PIPELINE_TYPE_NB> inputAssemblies = {};
    inputAssemblies[PIPELINE_TYPE::POINTS]          = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE};
    inputAssemblies[PIPELINE_TYPE::LINES]           = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_LINE_LIST, VK_FALSE};
    inputAssemblies[PIPELINE_TYPE::LINE_STRIPS]     = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_LINE_STRIP, VK_FALSE};
    inputAssemblies[PIPELINE_TYPE::TRIANGLES]       = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE};
    inputAssemblies[PIPELINE_TYPE::TRIANGLE_STRIPS] = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP, VK_FALSE};
    inputAssemblies[PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT] = inputAssemblies[PIPELINE_TYPE::TRIANGLE_STRIPS];
    inputAssemblies[PIPELINE_TYPE::TRIANGLE_FANS]   = {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, nullptr, 0, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN, VK_FALSE}; ;

    // Viewport State Fixed Function (Viewports and Scissors)
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) m_pSwapchainMgmt->m_extent.width;
    viewport.height = (float) m_pSwapchainMgmt->m_extent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = m_pSwapchainMgmt->m_extent;

    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    // Rasterizer Fixed Function
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = polygonMode;
    rasterizer.lineWidth = 1.0f;

    rasterizer.cullMode = VK_CULL_MODE_NONE;
    rasterizer.frontFace = rasterFrontFace;

    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    // Multisampling
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    // Color Blending Fixed Function
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;// | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_TRUE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;//SRC_ALPHA; // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;//_MINUS_SRC_ALPHA; // Optional
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    // Dynamic State Fixed Function, can be a nullptr if not used
    //VkDynamicState dynamicStates[] = {
    //  VK_DYNAMIC_STATE_VIEWPORT,
    //  VK_DYNAMIC_STATE_LINE_WIDTH
    //};

    //VkPipelineDynamicStateCreateInfo dynamicState = {};
    //dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    //dynamicState.dynamicStateCount = 2;
    //dynamicState.pDynamicStates = dynamicStates;

    VkStencilOpState stencilOpInfo = {};
    stencilOpInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    stencilOpInfo.failOp = VK_STENCIL_OP_KEEP;
    stencilOpInfo.passOp = VK_STENCIL_OP_INCREMENT_AND_CLAMP;
    stencilOpInfo.depthFailOp = VK_STENCIL_OP_INCREMENT_AND_CLAMP;
    stencilOpInfo.compareMask = 1;
    stencilOpInfo.reference = 1;
    // Depth and Stencil testing Fixed Function
    VkPipelineDepthStencilStateCreateInfo depthStencilSolid = {};
    depthStencilSolid.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilSolid.depthTestEnable = VK_TRUE;
    depthStencilSolid.depthWriteEnable = VK_TRUE;
    depthStencilSolid.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilSolid.minDepthBounds = 0.0f;
    depthStencilSolid.maxDepthBounds = 0.0f;
    depthStencilSolid.stencilTestEnable = VK_TRUE;
    depthStencilSolid.front = stencilOpInfo;
    depthStencilSolid.back = {};

    VkPipelineDepthStencilStateCreateInfo depthStencilTransparent = depthStencilSolid;
    depthStencilTransparent.depthWriteEnable = VK_FALSE;
    depthStencilTransparent.stencilTestEnable = VK_FALSE;

    // Pipeline Layout
    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &shader.m_descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    pipelineLayoutInfo.pPushConstantRanges = 0; // Optional

    if (vkCreatePipelineLayout(m_pDeviceMgmt->getDevice(), &pipelineLayoutInfo, nullptr, &m_pipelineLayout) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create pipeline layout!");
    }

    std::array<VkGraphicsPipelineCreateInfo, PIPELINE_TYPE::PIPELINE_TYPE_NB> pipelineInfos = {};
    pipelineInfos[PIPELINE_TYPE::POINTS] = {
      VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,  // sType
      nullptr,                                          // pNext
      0,                                                // flags
      static_cast<uint32_t>(shaderStages.size()),       // stageCount
      shaderStages.data(),                              // pStages
      &vertexInputInfo,                                 // pVertexInputState
      &inputAssemblies[PIPELINE_TYPE::POINTS],          // pInputStateAssemplyState
      nullptr,                                          // pTesselationState
      &viewportState,                                   // pViewportState
      &rasterizer,                                      // pRasterizationState
      &multisampling,                                   // pMultisampleState
      &depthStencilSolid,                               // rDepthStencilState
      &colorBlending,                                   // pColorBlendState
      nullptr,                                          // pDynamicState
      m_pipelineLayout,                                 // layout
      renderPass,                                       // renderPass
      0,                                                // subpass
      VK_NULL_HANDLE,                                   // basePipelineHandle
      -1                                                // basePipelineIndex
    };
    pipelineInfos[PIPELINE_TYPE::LINES] = pipelineInfos[PIPELINE_TYPE::POINTS];
    pipelineInfos[PIPELINE_TYPE::LINES].basePipelineIndex = PIPELINE_TYPE::POINTS;
    pipelineInfos[PIPELINE_TYPE::LINES].pInputAssemblyState = &inputAssemblies[PIPELINE_TYPE::LINES];
    pipelineInfos[PIPELINE_TYPE::LINE_STRIPS] = pipelineInfos[PIPELINE_TYPE::POINTS];
    pipelineInfos[PIPELINE_TYPE::LINE_STRIPS].basePipelineIndex = PIPELINE_TYPE::POINTS;
    pipelineInfos[PIPELINE_TYPE::LINE_STRIPS].pInputAssemblyState = &inputAssemblies[PIPELINE_TYPE::LINE_STRIPS];

    pipelineInfos[PIPELINE_TYPE::TRIANGLES] = pipelineInfos[PIPELINE_TYPE::POINTS];
    pipelineInfos[PIPELINE_TYPE::TRIANGLES].basePipelineIndex = PIPELINE_TYPE::POINTS;
    pipelineInfos[PIPELINE_TYPE::TRIANGLES].pInputAssemblyState = &inputAssemblies[PIPELINE_TYPE::TRIANGLES];
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS] = pipelineInfos[PIPELINE_TYPE::POINTS];
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS].basePipelineIndex = PIPELINE_TYPE::POINTS;
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS].pInputAssemblyState = &inputAssemblies[PIPELINE_TYPE::TRIANGLE_STRIPS];
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_FANS] = pipelineInfos[PIPELINE_TYPE::POINTS];
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_FANS].basePipelineIndex = PIPELINE_TYPE::POINTS;
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_FANS].pInputAssemblyState = &inputAssemblies[PIPELINE_TYPE::TRIANGLE_FANS];

    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT] = pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS];
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT].basePipelineIndex = PIPELINE_TYPE::TRIANGLE_STRIPS;
    pipelineInfos[PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT].pDepthStencilState = &depthStencilTransparent;

    if (vkCreateGraphicsPipelines(m_pDeviceMgmt->getDevice(), VK_NULL_HANDLE, pipelineInfos.size(), pipelineInfos.data(), nullptr, m_graphicsPipelines.data()) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create graphics pipeline!");
    }

    // #TODO: Safely destroy modules that build the pipeline ?
    // Or not : they may be used elsewhere...
    //vkDestroyShaderModule(m_pDeviceMgmt->getDevice(), fragShaderModule, nullptr);
    //vkDestroyShaderModule(m_pDeviceMgmt->getDevice(), vertShaderModule, nullptr);
  }

  void cleanup()
  {
    for (auto& pipe : m_graphicsPipelines)
    {
      vkDestroyPipeline(m_pDeviceMgmt->getDevice(), pipe, nullptr);
    }
    vkDestroyPipelineLayout(m_pDeviceMgmt->getDevice(), m_pipelineLayout, nullptr);
  }
};
