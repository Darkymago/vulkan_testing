#pragma once

#include <vector>
#include <unordered_map>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

namespace Objects
{
  struct Vertex
  {
    glm::vec3 pos;
    glm::vec3 normal;
    glm::vec2 texCoord;
    glm::vec3 color;

    bool operator==(const Vertex& v) const
    {
      return (pos == v.pos) /*&& (normal == v.normal)*/ && (texCoord == v.texCoord) && (color == v.color);
    }
  };

  struct VPMatrices
  {
    glm::mat4 view;
    glm::mat4 proj;
  };

  struct ObjectHandle
  {
    size_t verticesOffset;
    size_t verticesSize;
    size_t indicesOffset;
    size_t indicesSize;
    std::vector<Vertex>* verticesArray = nullptr;
    std::vector<uint32_t>* indicesArray = nullptr;
  };

  struct ModelHandle
  {
    ObjectHandle m_obj;
    glm::mat4 model;
    //VPMatrices* m_pmvp;
  };
}

namespace std
{
  template<> struct hash<Objects::Vertex>
  {
    size_t operator()(Objects::Vertex const& vertex) const
    {
      return
        ((hash<glm::vec3>()(vertex.pos) ^
        (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
        (hash<glm::vec2>()(vertex.texCoord) << 1);
    }
  };
}

namespace Objects
{
  ObjectHandle LoadModel(const std::string& modelPath, std::vector<Vertex>& verticesArray, std::vector<uint32_t>& indicesArray)
  {
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, modelPath.c_str()))
    {
      throw std::runtime_error(err);
    }

    std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

    size_t vOffset = verticesArray.size();
    size_t iOffset = indicesArray.size();
    size_t vSize, iSize = 0;

    for (const auto& shape : shapes)
    {
      iSize += shape.mesh.indices.size();

      for (const auto& index : shape.mesh.indices)
      {
        Vertex vertex = {};

        vertex.pos = {
          attrib.vertices[3 * index.vertex_index + 0],
          attrib.vertices[3 * index.vertex_index + 1],
          attrib.vertices[3 * index.vertex_index + 2]
        };

        vertex.texCoord = {
          attrib.texcoords[2 * index.texcoord_index + 0],
          1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
        };

        vertex.color = {1.0f, 1.0f, 1.0f};

        if (uniqueVertices.count(vertex) == 0)
        {
          uniqueVertices[vertex] = static_cast<uint32_t>(verticesArray.size());
          verticesArray.push_back(vertex);
        }

        indicesArray.push_back(uniqueVertices[vertex]);
      }
    }
    vSize = uniqueVertices.size();

    return {vOffset, vSize, iOffset, iSize, &verticesArray, &indicesArray};
  }

}

