#pragma once

#include <iostream>
#include <cstring>
#include <stdexcept>
#include <vector>
#include <map>
#include <set>

#include "Common.h"

#include "HelperStructs.h"

class DeviceManager
{
public:
  VkInstance m_instance;
  DeviceSet m_deviceSet;

  struct {
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkQueue transferQueue;
  } m_queues;

  const std::vector<const char*>* m_pValidationLayers = nullptr;

private:
  const std::vector<const char*>* m_pDeviceExtensions = nullptr;

  VkDebugReportCallbackEXT m_callback;

public:
  VkPhysicalDevice getPhysicalDevice() const { return m_deviceSet.physicalDevice; }
  VkDevice getDevice() const { return m_deviceSet.device; }
  DeviceSet const& getDeviceSet() const { return m_deviceSet; }

  VkQueue getPresentQueue() const { return m_queues.presentQueue; }
  VkQueue getGraphicQueue() const { return m_queues.graphicsQueue; }
  VkQueue getCommandQueue() const { return m_queues.graphicsQueue; }

  void setDeviceExtensions(const std::vector<const char*>& deviceExtensions)
  {
    m_pDeviceExtensions = &deviceExtensions;
  }

  void setValidationLayers(const std::vector<const char*>& validationLayers)
  {
    m_pValidationLayers = &validationLayers;
  }
  bool areValidationLayersNull()
  {
    return !m_pValidationLayers;
  }

  void init(
      GLFWwindow* window,
      const char* appName, uint appMajorVersion, uint appMinorVersion, uint appPatchVersion,
      const char* engineName, uint engineMajorVersion, uint engineMinorVersion, uint enginePatchVersion)
  {
    createInstance(appName, appMajorVersion, appMinorVersion, appPatchVersion, engineName, engineMajorVersion, engineMinorVersion, enginePatchVersion);

    setupDebugCallback();

    createSurface(window);

    pickPhysicalDevice();
    createLogicalDevice();
  }

  void cleanupDevice()
  {
    vkDestroyDevice(m_deviceSet.device, nullptr);

    vkDestroySurfaceKHR(m_instance, m_deviceSet.surface, nullptr);
    DestroyDebugReportCallbackEXT(m_instance, m_callback, nullptr);
    vkDestroyInstance(m_instance, nullptr);
  }

private:
  void createInstance(
      const char* appName, uint appMajorVersion, uint appMinorVersion, uint appPatchVersion,
      const char* engineName, uint engineMajorVersion, uint engineMinorVersion, uint enginePatchVersion)
  {
    if (m_pValidationLayers && !checkValidationLayerSupport())
    {
      throw std::runtime_error("validation layers requested, but not available!");
    }

    // Creating Vulkan Application Info needed by Vulkan Instance Creation Info
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = appName;
    appInfo.applicationVersion = VK_MAKE_VERSION(appMajorVersion, appMinorVersion, appPatchVersion);
    appInfo.pEngineName = engineName;
    appInfo.engineVersion = VK_MAKE_VERSION(engineMajorVersion, engineMinorVersion, enginePatchVersion);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    // Creating Vulkan Instance Creation Info
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    // Adding GLFW and Validation Layer Debug Report (if enabled) extensions to Vulkan Instance Creation Info
    auto extensions = getRequiredExtensions();
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    // Adding Validation Layers info if enabled
    if (m_pValidationLayers)
    {
      createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers->size());
      createInfo.ppEnabledLayerNames = m_pValidationLayers->data();
    }
    else
    {
      createInfo.enabledLayerCount = 0;
    }

    // Creating Vulkan Instance from the info
    VkResult res = vkCreateInstance(&createInfo, nullptr, &m_instance);
    if (res != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create instance!");
    }
  }

  void setupDebugCallback()
  {
    if (!m_pValidationLayers) return;

    VkDebugReportCallbackCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback = debugCallback;

    if (CreateDebugReportCallbackEXT(m_instance, &createInfo, nullptr, &m_callback))
    {
      throw std::runtime_error("failed to set up debug callback!");
    }
  }

  static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
      VkDebugReportFlagsEXT flags,
      VkDebugReportObjectTypeEXT objType,
      uint64_t obj,
      size_t location,
      int32_t code,
      const char* layerPrefix,
      const char* msg,
      void* userData)
  {
    std::cerr << "Validation Layer: " << msg << std::endl;

    return VK_FALSE;
  }

  void createSurface(GLFWwindow* window)
  {
    if (glfwCreateWindowSurface(m_instance, window, nullptr, &m_deviceSet.surface) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create window surface!");
    }
  }

  std::vector<const char*> getRequiredExtensions()
  {
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if (m_pValidationLayers)
    {
      extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    return extensions;
  }

  void pickPhysicalDevice()
  {
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);

    if (deviceCount == 0)
    {
      throw std::runtime_error("failed to find GPUs with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(m_instance, &deviceCount, devices.data());

    std::multimap<int, VkPhysicalDevice> candidates;

    for (const auto& device : devices)
    {
      int score = rateDeviceSuitability(device);
      candidates.insert(std::make_pair(score, device));
    }

    if (candidates.rbegin()->first > 0)
    {
      m_deviceSet.physicalDevice = candidates.rbegin()->second;
    }
    else
    {
      throw std::runtime_error("failed to find a suitable GPU!");
    }
  }

  void createLogicalDevice()
  {
    QueueFamilyIndices indices = QueueFamilyIndices::GetQueueFamilies(m_deviceSet);

    // Basic Queue Creation Info
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = {indices.graphicsFamily.value(), indices.presentFamily.value()}; // #TODO: get back transfer queue ! //, indices.transferFamily.value()};

    float queuePriority = 1.0f;
    for (int queueFamily : uniqueQueueFamilies)
    {
      VkDeviceQueueCreateInfo queueCreateInfo = {};
      queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
      queueCreateInfo.queueFamilyIndex = queueFamily;
      queueCreateInfo.queueCount = 1;
      queueCreateInfo.pQueuePriorities = &queuePriority;
      queueCreateInfos.push_back(queueCreateInfo);
    }

    // Physical Features Needed for the Queues
    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    deviceFeatures.fillModeNonSolid = VK_TRUE;
    deviceFeatures.shaderClipDistance = VK_TRUE;

    // Logical Device Creation
    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

    // Give Queues Info to Device Creation
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();

    createInfo.pEnabledFeatures = &deviceFeatures;

    // Give Extensions Info to Device Creation
    auto pDeviceExtensions = getDeviceExtensions();
    if (pDeviceExtensions)
    {
      createInfo.enabledExtensionCount = static_cast<uint32_t>(pDeviceExtensions->size());
      createInfo.ppEnabledExtensionNames = pDeviceExtensions->data();
    }

    // Enable Validation Layers if wanted
    if (m_pValidationLayers)
    {
      createInfo.enabledLayerCount = static_cast<uint32_t>(m_pValidationLayers->size());
      createInfo.ppEnabledLayerNames = m_pValidationLayers->data();
    }
    else {
      createInfo.enabledLayerCount = 0;
    }

    if (vkCreateDevice(m_deviceSet.physicalDevice, &createInfo, nullptr, &m_deviceSet.device) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create logical device!");
    }

    vkGetDeviceQueue(m_deviceSet.device, indices.graphicsFamily.value(), 0, &m_queues.graphicsQueue);
    vkGetDeviceQueue(m_deviceSet.device, indices.presentFamily.value(), 0, &m_queues.presentQueue);
  }

  // ##### Helpers #####
  int rateDeviceSuitability(VkPhysicalDevice device)
  {
    // Basic Device Suitability Check
    VkPhysicalDeviceProperties deviceProperties;
    VkPhysicalDeviceFeatures deviceFeatures;
    vkGetPhysicalDeviceProperties(device, &deviceProperties);
    vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    int score = 0;

    // Favoritize Discrete GPUs
    if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
    {
      score += 1000;
    }

    // Maximum Texture Size as limitant
    score += deviceProperties.limits.maxImageDimension2D;

    QueueFamilyIndices indices = QueueFamilyIndices::GetQueueFamilies(device, m_deviceSet.surface);

    bool extensionsSupported = checkDeviceExtensionSupport(device);
    bool swapChainAdequate = false;

    if (extensionsSupported)
    {
      SwapChainSupportDetails swapChainSupport = SwapChainSupportDetails::QuerySwapChainSupport(device, m_deviceSet.surface);
      swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
    }

    if (!(deviceFeatures.geometryShader && indices.isComplete() && extensionsSupported && swapChainAdequate && deviceFeatures.samplerAnisotropy))
    {
      return 0;
    }

    return score;
  }

  bool checkDeviceExtensionSupport(VkPhysicalDevice device)
  {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

    const std::vector<const char*>& deviceExtensions = *m_pDeviceExtensions;
    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

    for (const auto& extension : availableExtensions)
    {
      requiredExtensions.erase(extension.extensionName);
    }

    return requiredExtensions.empty();
  }

  const std::vector<const char*>* getDeviceExtensions() const
  {
    return m_pDeviceExtensions ? m_pDeviceExtensions : nullptr;
  }

  // ##### Validation Layers Helpers #####
  bool checkValidationLayerSupport()
  {
    // Getting Available Validation Layers
    // getting available validation layers count
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    // getting available validation layers properties
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    // Displaying Available Validation Layers
    std::cout << "Available Validation Layers:" << std::endl;
    for (const auto& vl : availableLayers)
    {
      std::cout << "\t" << vl.layerName << std::endl;
    }

    // Confront Requested VL to Availables ones
    for (const char* layerName : *m_pValidationLayers)
    {
      bool layerFound = false;

      for (const auto& layerProperties : availableLayers)
      {
        if (strcmp(layerName, layerProperties.layerName) == 0)
        {
          layerFound = true;
        }
      }

      if (!layerFound)
      {
        return false;
      }
    }

    return true;
  }

  static VkResult CreateDebugReportCallbackEXT(
      VkInstance instance,
      const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
      const VkAllocationCallbacks* pAllocator,
      VkDebugReportCallbackEXT* pCallback)
  {
    auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    if (func != nullptr)
    {
      return func(instance, pCreateInfo, pAllocator, pCallback);
    }
    else
    {
      return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
  }

  static void DestroyDebugReportCallbackEXT(
      VkInstance instance,
      VkDebugReportCallbackEXT callback,
      const VkAllocationCallbacks* pAllocator)
  {
    auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    if (func != nullptr)
    {
      func(instance, callback, pAllocator);
    }
  }

protected:
};
