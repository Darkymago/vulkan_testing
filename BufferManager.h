#pragma once

#include <vector>

#include "Common.h"

#include "BufferHelpers.h"

#include "DeviceManager.h"

struct BufferHandle
{
  size_t index = -1;
  size_t offset = 0;

  bool bSet = false;

  bool isSet() const { return bSet; }
  void set(size_t idx, size_t offset = 0)
  {
    index = idx;
    offset = offset;
    bSet = true;
  }
  void unset()
  {
    index = -1;
    bSet = false;
  }
};

struct BufferManager
{
protected:
  std::vector<Buffers::BufferWrapper> m_buffers;

  DeviceManager const* m_pDeviceMgmt;

public:
  ~BufferManager(){}

  void init(DeviceManager const& deviceManager)
  {
    m_pDeviceMgmt = &deviceManager;
  }

  VkBuffer getBuffer(const BufferHandle& handle) const
  {
    return m_buffers[handle.index].buffer;
  }

  void createBuffer(
      VkDeviceSize bufferSize,
      void* data,
      VkCommandPool commandPool,
      VkBufferUsageFlagBits flag,
      BufferHandle& handle)
  {
    if (!handle.isSet())
    {
      handle.index = m_buffers.size();
      m_buffers.emplace_back();

      handle.bSet = true;
    }

    Buffers::BufferWrapper& newWrapper = m_buffers.at(handle.index);
    ++(newWrapper.handleCount);

    Buffers::CreateViaStagingBuffer(
        m_pDeviceMgmt->getDeviceSet(),
        bufferSize,
        data,
        commandPool,
        m_pDeviceMgmt->getCommandQueue(),
        newWrapper,
        flag
    );
  }
  void createUniformBuffer(VkDeviceSize bufferSize, BufferHandle& handle)
  {
    if (!handle.isSet())
    {
      handle.set(m_buffers.size());
      m_buffers.emplace_back();
    }

    Buffers::BufferWrapper& newWrapper = m_buffers.at(handle.index);
    ++(newWrapper.handleCount);

    Buffers::CreateBuffer(
        m_pDeviceMgmt->getDeviceSet(),
        bufferSize,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        newWrapper.buffer,
        newWrapper.memory
    );
  }

  void updateBuffer(BufferHandle& handle, VkDeviceSize size, void* data, VkCommandPool commandPool = VK_NULL_HANDLE)
  {
    Buffers::BufferWrapper& wrapper = m_buffers.at(handle.index);

    if ((wrapper.stagingMemory != VK_NULL_HANDLE) and (commandPool != VK_NULL_HANDLE))
    {
      Buffers::UpdateViaStagingBuffer(m_pDeviceMgmt->getDeviceSet(), commandPool, m_pDeviceMgmt->getCommandQueue(), wrapper, size, data);
    }
    else
    {
      Buffers::UpdateBuffer(m_pDeviceMgmt->getDeviceSet(), wrapper, size, data);
    }
  }

  void cleanBuffer(BufferHandle& handle)
  {
    if (!handle.isSet()) { return; }

    Buffers::BufferWrapper& wrapper = m_buffers.at(handle.index);
    handle.unset();
    --wrapper.handleCount;

    if (wrapper.handleCount == 0)
    {
      vkDestroyBuffer(m_pDeviceMgmt->getDevice(), wrapper.buffer, nullptr);
      vkFreeMemory(m_pDeviceMgmt->getDevice(), wrapper.memory, nullptr);
      vkDestroyBuffer(m_pDeviceMgmt->getDevice(), wrapper.stagingBuffer, nullptr);
      vkFreeMemory(m_pDeviceMgmt->getDevice(), wrapper.stagingMemory, nullptr);

      wrapper = {};
    }
  }
};
