#pragma once

#include <vector>
#include <functional>
#include <optional>

#include "Common.h"

struct DeviceSet
{
  VkSurfaceKHR surface = VK_NULL_HANDLE;

  VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
  VkDevice device = VK_NULL_HANDLE;
};

struct QueueFamilyIndices
{
  std::optional<uint32_t> graphicsFamily;
  std::optional<uint32_t> computeFamily;
  std::optional<uint32_t> presentFamily;
  std::optional<uint32_t> transferFamily;
  std::optional<uint32_t> sparseBindingFamily;

  bool isComplete()
  {
    return graphicsFamily.has_value() && presentFamily.has_value(); // && transferFamily >= 0;
  }

  static QueueFamilyIndices& GetQueueFamilies(const DeviceSet& deviceSet)
  {
    return GetQueueFamilies(deviceSet.physicalDevice, deviceSet.surface);
  }

  template<typename... Args>
  static QueueFamilyIndices& GetQueueFamilies(Args... args)
  {
    static auto onceFunction = std::bind(CreateQueueFamilies<Args...>, args...);
    return Apply( onceFunction );
  }

private:
  static QueueFamilyIndices& Apply(const std::function<QueueFamilyIndices&()>& function)
  {
    static QueueFamilyIndices& indices = function();
    return indices;
  }

  template<typename... Args>
  static QueueFamilyIndices& CreateQueueFamilies(Args... args)
  {
    static QueueFamilyIndices indices = CreateQueueFamiliesOnce(std::forward<Args>(args)...);
    return indices;
  }

  static QueueFamilyIndices CreateQueueFamiliesOnce(const VkPhysicalDevice& device, const VkSurfaceKHR& surface)
  {
    QueueFamilyIndices indices;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto& queueFamily : queueFamilies)
    {
      // Graphics Family identification
      if ((indices.graphicsFamily < 0) && (queueFamily.queueCount > 0) && (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT))
      {
        indices.graphicsFamily = i;
      }

      // Compute Family identification
      if ((indices.computeFamily < 0) && (queueFamily.queueCount > 0) && (queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT))
      {
        indices.computeFamily = i;
      }

      // Transfer Family identification
      if ((indices.transferFamily < 0) && (queueFamily.queueCount > 0) && (queueFamily.queueFlags & VK_QUEUE_TRANSFER_BIT))
      {
        if (i != indices.graphicsFamily && i != indices.computeFamily)
        {
          indices.transferFamily = i;
        }
      }

      // Surface Support KHR identification
      VkBool32 presentSupport = false;
      vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

      if (queueFamily.queueCount > 0 && presentSupport)
      {
        indices.presentFamily = i;
      }

      if (indices.isComplete())
      {
        break;
      }

      ++i;
    }

    if (!indices.isComplete())
    {
      if (indices.transferFamily < 0)
      {
        indices.transferFamily = indices.graphicsFamily;
      }
    }

    return indices;
  }
};

struct SwapChainSupportDetails
{
  VkSurfaceCapabilitiesKHR capabilities;
  std::vector<VkSurfaceFormatKHR> formats;
  std::vector<VkPresentModeKHR> presentModes;

  static SwapChainSupportDetails QuerySwapChainSupport(const DeviceSet& deviceSet)
  {
    return QuerySwapChainSupport(deviceSet.physicalDevice, deviceSet.surface);
  }
  static SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface)
  {
    SwapChainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
    if (formatCount != 0)
    {
      details.formats.resize(formatCount);
      vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
    if (presentModeCount != 0)
    {
      details.presentModes.resize(presentModeCount);
      vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
    }

    return details;
  }
};
