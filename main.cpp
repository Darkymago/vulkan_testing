// #define GLFW_INCLUDE_VULKAN #include <GLFW/glfw3.h>

#include "TutorialApp.h"

int main()
{
  TutorialApplication app;

  try
  {
    app.run();
  }
  catch (const std::runtime_error& e)
  {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
