#pragma once

#include <stdexcept>
#include <cstring>

#include "Common.h"

#include "HelperStructs.h"
#include "CommandHelpers.h"

namespace Buffers
{
  uint32_t FindMemoryType(VkPhysicalDevice device, uint32_t typeFilter, VkMemoryPropertyFlags properties)
  {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(device, &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
    {
      if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
      {
        return i;
      }
    }

    throw std::runtime_error("failed to find suitablememory type!");
  }

  struct BufferWrapper
  {
    VkBuffer buffer = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkBuffer stagingBuffer = VK_NULL_HANDLE;
    VkDeviceMemory stagingMemory = VK_NULL_HANDLE;

    size_t handleCount = 0;
  };

  void CreateBuffer(
      DeviceSet const& deviceSet,
      VkDeviceSize size,
      VkBufferUsageFlags usage,
      VkMemoryPropertyFlags properties,
      VkBuffer& buffer,
      VkDeviceMemory& bufferMemory)
  {
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(deviceSet.device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create vertex buffer!");
    }

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(deviceSet.device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = FindMemoryType(deviceSet.physicalDevice, memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(deviceSet.device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to allocate vertex buffer memory!");
    }

    vkBindBufferMemory(deviceSet.device, buffer, bufferMemory, 0);
  }
  void CreateBuffer(
      DeviceSet const& deviceSet,
      VkDeviceSize size,
      VkBufferUsageFlags usage,
      VkMemoryPropertyFlags properties,
      BufferWrapper& handle)
  {
    CreateBuffer(deviceSet, size, usage, properties, handle.buffer, handle.memory);
  }

  void CopyBuffer(VkDevice device, VkCommandPool commandPool, VkQueue commandQueue, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
  {
    VkCommandBuffer commandBuffer = Command::BeginSingleTimeCommands(device, commandPool);

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0; // Optional
    copyRegion.dstOffset = 0; // Optional
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    Command::SubmitSingleTimeCommands(device, commandPool, commandQueue, commandBuffer);
  }

  void CreateViaStagingBuffer(DeviceSet const& deviceSet, VkDeviceSize bufferSize, void* dataContainer, VkCommandPool commandPool, VkQueue commandQueue, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkBufferUsageFlagBits usage, VkBuffer& stagingBuffer, VkDeviceMemory& stagingBufferMemory)
  {
    Buffers::CreateBuffer(
        deviceSet,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    void* data;
    vkMapMemory(deviceSet.device, stagingBufferMemory, 0, bufferSize, 0, &data);
    {
      memcpy(data, dataContainer, (size_t) bufferSize);
    }
    vkUnmapMemory(deviceSet.device, stagingBufferMemory);

    Buffers::CreateBuffer(
        deviceSet,
        bufferSize,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        buffer,
        bufferMemory
    );

    CopyBuffer(deviceSet.device, commandPool, commandQueue, stagingBuffer, buffer, bufferSize);
  }
  void CreateViaStagingBuffer(DeviceSet const& deviceSet, VkDeviceSize bufferSize, void* dataContainer, VkCommandPool commandPool, VkQueue commandQueue, VkBuffer& buffer, VkDeviceMemory& bufferMemory, VkBufferUsageFlagBits usage)
  {
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    CreateViaStagingBuffer(deviceSet, bufferSize, dataContainer, commandPool, commandQueue, buffer, bufferMemory, usage, stagingBuffer, stagingBufferMemory);
  }
  void CreateViaStagingBuffer(DeviceSet const& deviceSet, VkDeviceSize bufferSize, void* dataContainer, VkCommandPool commandPool, VkQueue commandQueue, BufferWrapper& handle, VkBufferUsageFlagBits usage)
  {
    CreateViaStagingBuffer(deviceSet, bufferSize, dataContainer, commandPool, commandQueue, handle.buffer, handle.memory, usage, handle.stagingBuffer, handle.stagingMemory);
  }

  void UpdateBuffer(DeviceSet const& deviceSet, VkBuffer& buffer, VkDeviceMemory& memory, VkDeviceSize size, void* data)
  {
    void* gpuData;
    vkMapMemory(deviceSet.device, memory, 0, size, 0, &gpuData);
    {
      memcpy(gpuData, data, size);
    }
    vkUnmapMemory(deviceSet.device, memory);
  }
  void UpdateBuffer(DeviceSet const& deviceSet, BufferWrapper& handle, VkDeviceSize size, void* data)
  {
    UpdateBuffer(deviceSet, handle.buffer, handle.memory, size, data);
  }

  void UpdateViaStagingBuffer(DeviceSet const& deviceSet, VkCommandPool commandPool, VkQueue commandQueue, VkBuffer& buffer, VkDeviceMemory& memory, VkBuffer& stagingBuffer, VkDeviceMemory& stagingMemory, VkDeviceSize size, void* data)
  {
    void* gpuData;
    vkMapMemory(deviceSet.device, stagingMemory, 0, size, 0, &gpuData);
    {
      memcpy(gpuData, data, size);
    }
    vkUnmapMemory(deviceSet.device, stagingMemory);

    CopyBuffer(deviceSet.device, commandPool, commandQueue, stagingBuffer, buffer, size);
  }
  void UpdateViaStagingBuffer(DeviceSet const& deviceSet, VkCommandPool commandPool, VkQueue commandQueue, BufferWrapper& handle, VkDeviceSize size, void* data)
  {
    UpdateViaStagingBuffer(deviceSet, commandPool, commandQueue, handle.buffer, handle.memory, handle.stagingBuffer, handle.stagingMemory, size, data);
  }
}
