#pragma once

#include <iostream>
#include <stdexcept>
#include <vector>
#include <cstring>
#include <algorithm>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include <chrono>

#include "ImageHelpers.h"

#include "DeviceManager.h"
#include "SwapchainManager.h"
#include "PipelineManager.h"
#include "TextureManager.h"
#include "DescriptorManager.h"
#include "BufferManager.h"

const int WIDTH = 600;
const int HEIGHT = 400;

const std::vector<const char*> validationLayers = {
  "VK_LAYER_KHRONOS_validation"
};
const std::vector<const char*> deviceExtensions = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

class SimpleApplication
{
protected:
  // ##### Variables #####
  GLFWwindow* p_window;

  DeviceManager m_deviceMgmt;
  SwapchainManager m_swapchainMgmt;
  PipelineManager m_pipelineMgmt;
  TextureManager m_textureMgmt;
  DescriptorManager m_descriptorMgmt;
  BufferManager m_bufferMgmt;

  std::vector<VkFramebuffer> swapChainFramebuffers;

  VkRenderPass vkRenderPass;

  VkCommandPool vkCommandPool;
  std::vector<VkCommandBuffer> commandBuffers;
  uint32_t uiCurrentBuffer;

  struct {
    VkSemaphore imageAvailable;
    VkSemaphore renderFinished;
  } semaphores;

  VkImage depthImage;
  VkDeviceMemory depthImageMemory;
  VkImageView depthImageView;

  bool bVertexBuffersToUpdate = true;

public:
  ~SimpleApplication() {}

  // ##### Run Function #####
  void run()
  {
    initWindow();
    initVulkan();
    initCamera();
    initWorld();
    mainLoop();
    cleanup();
  }

private:
  // ##### Run Helpers #####
  void initWindow()
  {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    p_window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
    glfwSetWindowUserPointer(p_window, this);

    // Set Callbacks
    glfwSetKeyCallback(p_window, key_callback);
    glfwSetMouseButtonCallback(p_window, mouse_button_callback);
    glfwSetCursorPosCallback(p_window, cursor_position_callback);
    glfwSetScrollCallback(p_window, scroll_callback);
    glfwSetWindowRefreshCallback(p_window, window_refresh_callback);
  }

  void initVulkan()
  {
    m_deviceMgmt.setDeviceExtensions(deviceExtensions);
    m_deviceMgmt.setValidationLayers(validationLayers);
    m_deviceMgmt.init(p_window, "Vulkan Tryouts", 0, 1, 0, "No Engine", 0, 1, 0);

    m_swapchainMgmt.init(p_window, m_deviceMgmt);
    m_pipelineMgmt.init(m_deviceMgmt, m_swapchainMgmt);
    m_textureMgmt.init(m_deviceMgmt);
    m_descriptorMgmt.init(m_deviceMgmt);
    m_bufferMgmt.init(m_deviceMgmt);

    createRenderPass();

    createCommandPool();


    // Shader objects init
    initShaders();
    createShadersPipes();

    createDepthResources();
    createFramebuffers();

    createSemaphores();
    createCommandBuffers();
  }
  virtual void initShaders() = 0;
  virtual void initCamera() = 0;
  virtual void initWorld() = 0;

  void mainLoop()
  {
    glfwSetKeyCallback(p_window, key_callback);

    while (!glfwWindowShouldClose(p_window))
    {
      glfwPollEvents();

      updateBuffers();
      drawFrame();
    }

    vkDeviceWaitIdle(m_deviceMgmt.getDevice());
  }

  void cleanup()
  {
    cleanupToSwap();
    cleanupShaders();

    m_textureMgmt.cleanup();
    m_descriptorMgmt.cleanup();

    cleanupBuffers();

    vkDestroySemaphore(m_deviceMgmt.getDevice(), semaphores.renderFinished, nullptr);
    vkDestroySemaphore(m_deviceMgmt.getDevice(), semaphores.imageAvailable, nullptr);

    vkDestroyCommandPool(m_deviceMgmt.getDevice(), vkCommandPool, nullptr);

    m_deviceMgmt.cleanupDevice();

    glfwDestroyWindow(p_window);
    glfwTerminate();
  }
  virtual void cleanupShaders() = 0;
  virtual void cleanupBuffers() = 0;

  void createRenderPass()
  {
    // Color Attachment Description
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = m_swapchainMgmt.m_imageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Depth Attachment Description
    VkAttachmentDescription depthAttachment = {};
    depthAttachment.format = Images::FindDepthFormat(m_deviceMgmt.getPhysicalDevice());;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;

    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    // Create Render Pass
    std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;

    // Subpass Dependencies
    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;

    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;

    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vkCreateRenderPass(m_deviceMgmt.getDevice(), &renderPassInfo, nullptr, &vkRenderPass) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create render pass!");
    }
  }

  virtual void createShadersPipes() = 0;

  void createFramebuffers()
  {
    swapChainFramebuffers.resize(m_swapchainMgmt.m_swapchainImageViews.size());

    for (size_t i = 0; i < m_swapchainMgmt.m_swapchainImageViews.size(); ++i)
    {
      std::array<VkImageView, 2> attachments = {
        m_swapchainMgmt.m_swapchainImageViews[i],
        depthImageView
      };

      VkFramebufferCreateInfo framebufferInfo = {};
      framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
      framebufferInfo.renderPass = vkRenderPass;
      framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
      framebufferInfo.pAttachments = attachments.data();
      framebufferInfo.width = m_swapchainMgmt.m_extent.width;
      framebufferInfo.height = m_swapchainMgmt.m_extent.height;
      framebufferInfo.layers = 1;

      if (vkCreateFramebuffer(m_deviceMgmt.getDevice(), &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to create framebuffer!");
      }
    }
  }

  void createCommandPool()
  {
    QueueFamilyIndices& queueFamilyIndices = QueueFamilyIndices::GetQueueFamilies(m_deviceMgmt.getDeviceSet());

    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional

    if (vkCreateCommandPool(m_deviceMgmt.getDevice(), &poolInfo, nullptr, &vkCommandPool) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create command pool!");
    }
  }

  void createDepthResources()
  {
    VkFormat depthFormat = Images::FindDepthFormat(m_deviceMgmt.getPhysicalDevice());

    Images::CreateImage(
        m_deviceMgmt.getDeviceSet(),
        m_swapchainMgmt.m_extent.width,
        m_swapchainMgmt.m_extent.height,
        depthFormat,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        depthImage,
        depthImageMemory
    );
    depthImageView = Images::CreateImageView(m_deviceMgmt.getDeviceSet(), depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

    Images::TransitionImageLayout(m_deviceMgmt.getDevice(), vkCommandPool, m_deviceMgmt.m_queues.graphicsQueue, depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
  }

  void createCommandBuffers()
  {
    commandBuffers.resize(swapChainFramebuffers.size());

    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = vkCommandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t) commandBuffers.size();

    if (vkAllocateCommandBuffers(m_deviceMgmt.getDevice(), &allocInfo, commandBuffers.data()) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to allocate command buffers!");
    }
  }

  virtual void draw(const VkCommandBuffer&) = 0;
protected:
  void updateCommandBuffers()
  {
    for (size_t i = 0; i < commandBuffers.size(); ++i)
    {
      VkCommandBufferBeginInfo beginInfo = {};
      beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
      beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
      beginInfo.pInheritanceInfo = nullptr; // Optional

      if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to begin recording command buffer");
      }

      VkRenderPassBeginInfo renderPassInfo = {};
      renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
      renderPassInfo.renderPass = vkRenderPass;
      renderPassInfo.framebuffer = swapChainFramebuffers[i];

      renderPassInfo.renderArea.offset = {0, 0};
      renderPassInfo.renderArea.extent = m_swapchainMgmt.m_extent;

      std::array<VkClearValue, 2> clearValues = {};
      clearValues[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
      clearValues[1].depthStencil = {1.0f, 0};

      renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
      renderPassInfo.pClearValues = clearValues.data();

      vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
      {
        draw(commandBuffers[i]);
      }
      vkCmdEndRenderPass(commandBuffers[i]);

      if(vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to record command buffer!");
      }
    }
  }

private:
  void createSemaphores()
  {
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType= VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    if (
        vkCreateSemaphore(m_deviceMgmt.getDevice(), &semaphoreInfo, nullptr, &semaphores.imageAvailable) != VK_SUCCESS ||
        vkCreateSemaphore(m_deviceMgmt.getDevice(), &semaphoreInfo, nullptr, &semaphores.renderFinished) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create semaphores!");
    }
  }

  // ##### Main Loop Helpers #####
  void drawFrame()
  {
    prepareFrame();

    // Command Buffer Submission
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &semaphores.imageAvailable;
    submitInfo.pWaitDstStageMask = waitStages;

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers[uiCurrentBuffer];

    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &semaphores.renderFinished;

    if (vkQueueSubmit(m_deviceMgmt.m_queues.graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to submit draw command buffer!");
    }

    submitFrame();
  }

  void prepareFrame()
  {
    // Image Acquisition from SwapChain
    VkResult res = vkAcquireNextImageKHR(
        m_deviceMgmt.getDevice(),
        m_swapchainMgmt.m_swapchain,
        std::numeric_limits<uint64_t>::max(),
        semaphores.imageAvailable,
        VK_NULL_HANDLE,
        &uiCurrentBuffer
    );

    if (res == VK_ERROR_OUT_OF_DATE_KHR)
    {
      recreateFromSwap();
      return;
    }
    else if (res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR)
    {
      throw std::runtime_error("failed to acquire swap chain image!");
    }
  }

  void submitFrame()
  {
    // Presentation
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &semaphores.renderFinished;

    VkSwapchainKHR swapChains[] = {m_swapchainMgmt.m_swapchain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &uiCurrentBuffer;

    presentInfo.pResults = nullptr; // Optional

    VkResult res = vkQueuePresentKHR(m_deviceMgmt.m_queues.presentQueue, &presentInfo);

    if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR)
    {
      recreateFromSwap();
    }
    else if (res != VK_SUCCESS)
    {
      throw std::runtime_error("failed to present swap chain image!");
    }

    vkQueueWaitIdle(m_deviceMgmt.m_queues.presentQueue);
  }

  virtual void updateBuffers() = 0;

  // ##### Cleanup Helpers #####
  void cleanupToSwap()
  {
    vkDestroyImageView(m_deviceMgmt.getDevice(), depthImageView, nullptr);
    vkDestroyImage(m_deviceMgmt.getDevice(), depthImage, nullptr);
    vkFreeMemory(m_deviceMgmt.getDevice(), depthImageMemory, nullptr);

    for (auto framebuffer : swapChainFramebuffers)
    {
      vkDestroyFramebuffer(m_deviceMgmt.getDevice(), framebuffer, nullptr);
    }

    vkFreeCommandBuffers(m_deviceMgmt.getDevice(), vkCommandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

    m_pipelineMgmt.cleanup();
    vkDestroyRenderPass(m_deviceMgmt.getDevice(), vkRenderPass, nullptr);

    m_swapchainMgmt.cleanup();
  }

  // ### Controls ###
  virtual void processKeyPress(int key, int mods) = 0;
  virtual void processMouseClick(int button, int state, double x, double y) = 0;
  virtual void processMouseMotion(double x, double y) = 0;
  virtual void processMouseWheel(double xoffset, double yoffset) = 0;

  // ##### Callbacks #####
  // ### GLFW ###
  // --- Keyboard ---
  static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    SimpleApplication* app = reinterpret_cast<SimpleApplication*>(glfwGetWindowUserPointer(window));
    if (action == GLFW_PRESS)
    {
      app->processKeyPress(key, mods);
    }
  }
  static void mouse_button_callback(GLFWwindow* window, int button, int state, int mods)
  {
    SimpleApplication* app = reinterpret_cast<SimpleApplication*>(glfwGetWindowUserPointer(window));
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    app->processMouseClick(button, state, xpos, ypos);
  }
  static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
  {
    SimpleApplication* app = reinterpret_cast<SimpleApplication*>(glfwGetWindowUserPointer(window));
    app->processMouseMotion(xpos, ypos);
  }
  static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
  {
    SimpleApplication* app = reinterpret_cast<SimpleApplication*>(glfwGetWindowUserPointer(window));
    app->processMouseWheel(xoffset, yoffset);
  }
  static void window_refresh_callback(GLFWwindow* window)
  {
    SimpleApplication* app = reinterpret_cast<SimpleApplication*>(glfwGetWindowUserPointer(window));
    app->recreateFromSwap();
  }

  // ##### Generic Helpers #####
protected:
  void recreateFromSwap()
  {
    std::cout << "Recreating from Swap" << std::endl;
    int width, height;
    glfwGetWindowSize(p_window, &width, &height);
    if (width == 0 || height == 0) return;

    vkDeviceWaitIdle(m_deviceMgmt.getDevice());

    cleanupToSwap();

    m_swapchainMgmt.init(p_window, m_deviceMgmt);

    createRenderPass();

    createShadersPipes();

    createDepthResources();
    createFramebuffers();
    createCommandBuffers();

    updateCommandBuffers();
  }

private:
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
      VkDebugReportFlagsEXT flags,
      VkDebugReportObjectTypeEXT objType,
      uint64_t obj,
      size_t location,
      int32_t code,
      const char* layerPrefix,
      const char* msg,
      void* userData)
  {
    std::cerr << "Validation Layer: " << msg << std::endl;

    return VK_FALSE;
  }
};

