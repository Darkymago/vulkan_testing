#pragma once

#include "VulkanShaderHelper.h"
#include "VulkanObjLoader.h"

#include "VulkanPipelineManager.h"

#include <glm/glm.hpp>

struct SimpleTexturedObjShader : public Shaders::ShaderPipeAbstract<SimpleTexturedObjShader>
{
  typedef Objects::Vertex Vertex;
  const std::string VERTEX_SHADER_PATH = "shaders/shader.vert.spv";
  const std::string FRAGMENT_SHADER_PATH = "shaders/shader.frag.spv";

  static std::vector<VkVertexInputAttributeDescription> const Attributes;
  static std::vector<VkVertexInputBindingDescription> const Bindings;
  static std::vector<VkDescriptorSetLayoutBinding> const LayoutBindings;

  static std::vector<VkDescriptorBufferInfo> UniformInfos;
  static std::vector<VkDescriptorImageInfo> TextureInfos;

  // Uniforms
  Objects::MVPMatrices mvp;

  // Vertices
  std::vector<Vertex> m_vertices;
  std::vector<uint32_t> m_indices;

  // Instances
  std::vector<glm::mat4> m_models;

  struct
  {
    BufferHandle uniform;
    BufferHandle vertex;
    BufferHandle index;
    BufferHandle models;
  } buffers;

  void init(VulkanDeviceManager const& deviceManager, VulkanBufferManager& bufferManager)
  {
    ShaderPipeBase::init(deviceManager, bufferManager);

    loadMinimalGraphicsShaders(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
    createDescriptorSetLayout();
  }

  void add(glm::mat4 model)
  {
    m_models.push_back(model);
  }

  void draw(VkCommandBuffer commandBuffer, VulkanPipelineManager const& pipelineMgmt)
  {
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getCurrentPipeline());

    VkBuffer vertexBuffers[] = {ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.vertex), ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.models)};
    VkDeviceSize offsets[] = {0, 0};
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffers[0], &offsets[0]);
    vkCmdBindVertexBuffers(commandBuffer, 1, 1, &vertexBuffers[1], &offsets[1]);

    vkCmdBindIndexBuffer(commandBuffer, ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.index), 0, VK_INDEX_TYPE_UINT32);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.m_pipelineLayout, 0, 1, &static_cast<ShaderPipeBase*>(this)->m_descriptorSet, 0, nullptr);

    vkCmdDrawIndexed(commandBuffer, static_cast<uint32_t>(m_indices.size()), static_cast<uint32_t>(m_models.size()), 0, 0, 0);
  }

  void update()
  {
    ShaderPipeBase::m_pBufferMgmt->updateBuffer(buffers.uniform, sizeof(Objects::MVPMatrices), &mvp);
  }

  void clear(VkDevice device)
  {
    m_vertices.clear();
    m_indices.clear();
  }

  void setBuffers(VkCommandPool commandPool)
  {
      ShaderPipeBase::m_pBufferMgmt->createBuffer(
          sizeof(m_vertices[0])*m_vertices.size(),
          (void*) m_vertices.data(),
          commandPool,
          VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
          buffers.vertex
      );
      ShaderPipeBase::m_pBufferMgmt->createBuffer(
          sizeof(m_indices[0])*m_indices.size(),
          (void*) m_indices.data(),
          commandPool,
          VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
          buffers.index
      );
      ShaderPipeBase::m_pBufferMgmt->createBuffer(
          sizeof(m_models[0])*m_models.size(),
          (void*) m_models.data(),
          commandPool,
          VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
          buffers.models
      );
  };
  void setUniformHandle(BufferHandle& handle)
  {
    buffers.uniform = handle;
  }
  void setTextureInfo(VkImageView imageView, VkSampler sampler)
  {
    for (auto& info : TextureInfos)
    {
      info.sampler = sampler;
      info.imageView = imageView;
    }
  }

  void cleanBuffers()
  {
    m_pBufferMgmt->cleanBuffer(buffers.models);
    m_pBufferMgmt->cleanBuffer(buffers.vertex);
    m_pBufferMgmt->cleanBuffer(buffers.index);
    m_pBufferMgmt->cleanBuffer(buffers.uniform);
  }
};
const std::vector<VkVertexInputAttributeDescription> SimpleTexturedObjShader::Attributes = {
  // { location, binding, VkFormat, offset }
  {0, 0, VK_FORMAT_R32G32B32_SFLOAT, offset_of(&Vertex::pos)},
  {1, 0, VK_FORMAT_R32G32B32_SFLOAT, offset_of(&Vertex::normal)},
  {2, 0, VK_FORMAT_R32G32_SFLOAT, offset_of(&Vertex::texCoord)},
  {3, 0, VK_FORMAT_R32G32B32_SFLOAT, offset_of(&Vertex::color)},
  {4, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 0},
  {5, 1, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(glm::vec4)},
  {6, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 2*sizeof(glm::vec4)},
  {7, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 3*sizeof(glm::vec4)}
};
const std::vector<VkVertexInputBindingDescription> SimpleTexturedObjShader::Bindings = {
  // { binding, size, VkInputRate }
  {0, sizeof(Vertex), VK_VERTEX_INPUT_RATE_VERTEX},
  {1, sizeof(glm::mat4), VK_VERTEX_INPUT_RATE_INSTANCE}
};
const std::vector<VkDescriptorSetLayoutBinding> SimpleTexturedObjShader::LayoutBindings = {
  // { binding, VkDescriptorType, count, VkShaderStageFlags, pImmutableSamplers }
  {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT, nullptr},
  {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr}
};
std::vector<VkDescriptorBufferInfo> SimpleTexturedObjShader::UniformInfos = {
  // { VkHandle (Buffer to set after creation), offset, size }
  {VK_NULL_HANDLE, 0, sizeof(Objects::MVPMatrices)}
};
std::vector<VkDescriptorImageInfo> SimpleTexturedObjShader::TextureInfos = {
  // { view (VK_HANDLE), sampler (VK_HANDLE), flag }
  {VK_NULL_HANDLE, VK_NULL_HANDLE, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL}
};
