#pragma once

#include <iostream>
#include <stdexcept>
#include <vector>
#include <cstring>
#include <algorithm>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include <chrono>

#include "SimpleApplication.h"

//#include "shaders/InstancedColorPrimitiveShader.h"
#include "shaders/CelestialBodiesShader.h"

#include "models/teapot_data.h"
#include "models/GeometricShapes.h"

const std::string MODEL_PATH = "models/chalet.obj";
const std::string TEXTURE_PATH = "textures/chalet.jpg";

/* Axes
 * Y +
 *   |
 *   |   + Z
 *   |  /
 *   | /
 *   |/
 *   +---------+ X
 */
const std::vector<glm::vec4> axis_vertices = {
  { 0.0, 0.0, 0.0, 1.0 },
  { 1.0, 0.0, 0.0, 1.0 },
  { 0.0, 0.0, 0.0, 1.0 },
  { 0.0, 1.0, 0.0, 1.0 },
  { 0.0, 0.0, 0.0, 1.0 },
  { 0.0, 0.0, 1.0, 1.0 },
};
/* Cube
 *         p7           p6
 *        +------------+
 *       /|           /|
 *      / |          / |
 *  p4 /  |      p5 /  |
 *    +------------+   |
 *    |   |        |   |
 *    |   +--------|---+
 *    |  / p3      |  / p2    Z
 *    | /          | /        | Y
 *    |/           |/         |/
 *    +------------+          +--X
 *   p0           p1
 */
const std::vector<glm::vec4> cube_vertices = {
  { -0.5, -0.5,-0.5, 1.0 },
  {  0.5, -0.5,-0.5, 1.0 },
  {  0.5,  0.5,-0.5, 1.0 },
  { -0.5,  0.5,-0.5, 1.0 },
  { -0.5, -0.5, 0.5, 1.0 },
  {  0.5, -0.5, 0.5, 1.0 },
  {  0.5,  0.5, 0.5, 1.0 },
  { -0.5,  0.5, 0.5, 1.0 },
};

const std::vector<glm::vec4> plane_vertices = {
  {  0.5,  0.5, 0.0, 1.0 },
  {  0.5, -0.5, 0.0, 1.0 },
  { -0.5,  0.5, 0.0, 1.0 },
  { -0.5, -0.5, 0.0, 1.0 },
};
const std::vector<uint32_t> plane_indices = { 0, 1, 2, 3 };

const std::vector<glm::vec4> colors = {
  { 1.0, 0.0, 0.0, 1.0 }, // Red
  { 0.0, 1.0, 0.0, 1.0 }, // Green
  { 0.0, 0.0, 1.0, 1.0 }, // Blue
  { 1.0, 1.0, 1.0, 1.0 }, // White
  { 0.0, 1.0, 1.0, 1.0 }, // Cyan
  { 1.0, 0.0, 1.0, 1.0 }, // Magenta
  { 1.0, 1.0, 0.0, 1.0 }  // Yellow
};

class TutorialApplication : public SimpleApplication
{
private:
  // ##### Variables #####

  //SimpleTexturedObjShader m_shader;
  //PrimitiveShader m_shader;
  //InstancedColorPrimitiveShader m_shader;
  CelestialBodiesShader m_shader;
  typedef CelestialBodiesShader CurrentShader;

  Objects::ObjectHandle house;

  //struct {
  //  BufferHandle mvpHandle;
  //  BufferHandle clipPlaneHandle;
  //} uniformHandles;

  bool bVertexBuffersToUpdate = true;

  // Drawing Variables
  typedef CurrentShader::DrawInstance DrawInstance;
  typedef CurrentShader::DrawPart DrawPart;
  bool bShowAxes = true;
  std::array<size_t, 3> axesIndices;

  // Control variables
  // Camera Control
  int lastMouseX, lastMouseY;
  float thetaCam = 0.0, phiCam = 0.0;
  float distCam = 0.0;
  bool bMousePressed = false;

  const float thetaInit = 270.0, phiInit = 80.0, distInit = 20.0;

  // Model(s) Control
  bool bAnimating = false;
  bool bLookAtMode = true;
  VkPolygonMode polygonMode = VK_POLYGON_MODE_FILL;
  // Box Model
  float boxSize = 10.0f;

  // ###################
  // Thing Model Control
  // ###################
  size_t cubeIdx, teapotIdx, sphereIdx, legsIdx, wingsIdx;

  glm::vec4 thingColor = {0.0f, 1.0f, 0.0f, 0.0f};
  glm::vec3 thingPosition = {0.0, 0.0, 0.0};
  const glm::vec4 thingLegsColor = {0.5f, 0.0f, 0.5f, 0.0f};
  const glm::vec4 thingWingsColor = {0.5f, 0.5f, 0.0f, 0.0f};
  float thingScale, thingRotate, thingLegsAngle, thingWingsAngle;
  const float legLength = 1.0f, legWidth = 0.25f;
  const float wingLength = 1.0f, wingWidth = 0.4f, wingHeight = 0.1f;

  const float thingScaleInit = 1.0f;
  const float thingRotateInit = 0.0f;
  const float thingLegsAngleInit = 45.0f;
  const float thingWingsAngleInit = 0.0f;

  const glm::mat4 thingCubeConst = glm::scale(glm::vec3(1.0f, 1.0f, 1.0f));
  const glm::mat4 thingTeapotConst = glm::translate(glm::rotate(glm::scale(glm::vec3(0.25f, 0.25f, 0.25f)), glm::radians(90.0f), glm::vec3(1.0f, 0.0, 0.0f)), glm::vec3(0.0f, -2.0f, 0.0f));
  const glm::mat4 thingSphereConst = glm::scale(sqrt(2.0f) * glm::vec3(1.0f, 1.0f, 1.0f));

  glm::mat4 thingLegsCommon = glm::scale(glm::translate(thingScaleInit/2 * glm::vec3(1.0f, 0.0f, 0.0f)), glm::vec3(1.0f, 0.25f, 0.25f));
  const glm::mat4 thingLegsRot1 = glm::rotate(glm::radians(45.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  const glm::mat4 thingLegsRot2 = glm::rotate(glm::radians(45.0f +  90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  const glm::mat4 thingLegsRot3 = glm::rotate(glm::radians(45.0f + 180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  const glm::mat4 thingLegsRot4 = glm::rotate(glm::radians(45.0f + 270.0f), glm::vec3(0.0f, 0.0f, 1.0f));

  glm::mat4 thingWingsCommon = glm::scale(glm::translate(thingScaleInit/2 * glm::vec3(1.0f, 0.0f, 0.0f)),glm::vec3(1.0f, 0.4f, 0.1f));
  const glm::mat4 thingWingsRot1 = glm::rotate(glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
  const glm::mat4 thingWingsRot2 = glm::rotate(glm::radians(-90.0f), glm::vec3(0.0f, 0.0f, 1.0f));

  const uint8_t cubeModelFlag = 1, teapotModelFlag = 2, sphereModelFlag = 4;
  uint8_t modelSwitch = sphereModelFlag;
  bool bModelChange = true;

  // ####################
  // Solar System Control
  // ####################
  size_t sphereModel;
  // Sun
  size_t sunIdx;
  // Planets
#define DIST_REF (58) // in Mkm
#define RAD_REF (13.0) // in kkm
#define CALC_RAD(x) (0.1*x / sqrt(20  + 0.01*x*x))
  size_t mercuryIdx, mercuryTorusIdx;
  float mercuryDist = 58.0 / DIST_REF;
  float mercuryRad = CALC_RAD(5.0);
  float mercuryPer = 0.241;
  glm::vec4 mercuryColor = {0.4, 0.4, 0.4, 1.0};

  size_t venusIdx, venusTorusIdx;
  float venusDist = 108.0 / DIST_REF;
  float venusRad = CALC_RAD(12.0);
  float venusPer = 0.614;
  glm::vec4 venusColor = {0.7, 0.7, 0.7, 1.0};

  size_t earthIdx, earthTorusIdx;
  float earthDist = 150.0 / DIST_REF;
  float earthRad = CALC_RAD(13.0);
  float earthPer = 1.0;
  glm::vec4 earthColor = {0.0, 0.2, 0.9, 1.0};

  size_t marsIdx, marsTorusIdx;
  float marsDist = 228.0 / DIST_REF;
  float marsRad = CALC_RAD(7.0);
  float marsPer = 1.881;
  glm::vec4 marsColor = {0.9, 0.2, 0.2, 1.0};

  size_t jupiterIdx, jupiterTorusIdx;
  float jupiterDist = 778.0 / DIST_REF;
  float jupiterRad = CALC_RAD(143.0);
  float jupiterPer = 11.862;
  glm::vec4 jupiterColor = {0.9, 0.7, 0.2, 1.0};

  size_t saturnIdx, saturnTorusIdx;
  float saturnDist = 1433.0 / DIST_REF;
  float saturnRad = CALC_RAD(121.0);
  float saturnPer = 29.457;
  glm::vec4 saturnColor = {0.6, 0.9, 0.2, 1.0};

  size_t uranusIdx, uranusTorusIdx;
  float uranusDist = 2872.0 / DIST_REF;
  float uranusRad = CALC_RAD(51.0);
  float uranusPer = 84.021;
  glm::vec4 uranusColor = {0.4, 0.7, 0.8, 1.0};

  size_t neptuneIdx, neptuneTorusIdx;
  float neptuneDist = 4498.0 / DIST_REF;
  float neptuneRad = CALC_RAD(50.0);
  float neptunePer = 164.8;
  glm::vec4 neptuneColor = {0.2, 0.3, 0.9, 1.0};

public:
  ~TutorialApplication() {}

private:
  virtual void initShaders()
  {
    m_shader.init(m_deviceMgmt, m_bufferMgmt, vkCommandPool);

    //m_textureMgmt.addTexture(TEXTURE_PATH);
    //m_textureMgmt.sendTexturesToDevice(vkCommandPool);
    //m_textureMgmt.createTextureViews();
    //m_textureMgmt.createTextureSampler();

    //createUniformBuffers();
    //m_shader.setUniformBuffer(uniformHandles);
    //m_shader.setTextureView(m_textureMgmt.m_textures[0].imageView, m_textureMgmt.m_textureSampler);

    m_descriptorMgmt.createDescriptorPool(m_shader);
    m_descriptorMgmt.createDescriptorSet(m_shader);
    m_descriptorMgmt.updateDescriptorSet(m_shader);
  }
  virtual void createShadersPipes()
  {
    m_pipelineMgmt.createGraphicsPipeline(m_shader, vkRenderPass, polygonMode);
  }

  void initCamera()
  {
    thetaCam = thetaInit;
    phiCam = phiInit;
    distCam = distInit;
  }
  void initWorld()
  {
    //generateThingWorld();
    generateSolarSystemWorld();

    updateCommandBuffers();
  }

  void switchModel()
  {
    if (modelSwitch == sphereModelFlag)
      modelSwitch = cubeModelFlag;
    else
      modelSwitch = modelSwitch << 1;
  }

  virtual void cleanupShaders()
  {
    m_shader.cleanup();
  }
  virtual void cleanupBuffers()
  {
    //m_bufferMgmt.cleanBuffers(uniformHandles);

    m_shader.cleanBuffers();
  }

  virtual void draw(const VkCommandBuffer& commandBuffer)
  {
    m_shader.draw(commandBuffer, m_pipelineMgmt);
  }

  // Variable checks, updates and more
  void checkCameraVariables()
  {
    if (phiCam >= 180.0)
      phiCam = 180.0 - 0.00001;
    if (phiCam <= 0.0)
      phiCam = 0.0 + 0.00001;
    if (thetaCam >= 360.0)
      thetaCam -= 360.0;
    if (thetaCam < 0.0)
      thetaCam += 360.0;
  }
  void checkModelVariables()
  {
    if (thingRotate >= 360.0)
      thingRotate -= 360.0;
    if (thingRotate < 0.0)
      thingRotate += 360.0;
  }
  void updateBuffers()
  {
    glm::vec3 centerOffset = {0.0, 0.0, 0.0};
    checkCameraVariables();

    if (bLookAtMode)
    {
      m_shader.vp.view = glm::lookAt(
          distCam * glm::vec3(
            cos(glm::radians(thetaCam))*sin(glm::radians(phiCam)),
            sin(glm::radians(thetaCam))*sin(glm::radians(phiCam)),
            cos(glm::radians(phiCam))),
          centerOffset,
          {0.0, 0.0, 1.0}
          );
    }
    else
    {
      m_shader.vp.view = glm::mat4(1.0f);
      m_shader.vp.view = glm::translate(m_shader.vp.view, {0.0, 0.0, -distCam});
      m_shader.vp.view = glm::rotate(m_shader.vp.view, glm::radians(-phiCam), glm::vec3(1.0, 0.0, 0.0));
      m_shader.vp.view = glm::rotate(m_shader.vp.view, glm::radians(-90.0f-thetaCam), glm::vec3(0.0, 0.0, 1.0));
      m_shader.vp.view = glm::translate(m_shader.vp.view, -centerOffset);
    }

    //updateThingWorld();
    /*{
      static auto& cubeInstance = m_shader.getDraw(cubeIdx);
      static auto& teapotInstance = m_shader.getDraw(teapotIdx);
      static auto& sphereInstance = m_shader.getDraw(sphereIdx);
      static auto& legsInstance = m_shader.getDraw(legsIdx);
      static auto& wingsInstance = m_shader.getDraw(wingsIdx);

      cubeInstance.bActive = (cubeModelFlag == modelSwitch);
      teapotInstance.bActive = (teapotModelFlag == modelSwitch);
      sphereInstance.bActive = (sphereModelFlag == modelSwitch);
      auto& selectedInstance =
        cubeInstance.bActive ? cubeInstance :
        (teapotInstance.bActive ? teapotInstance : sphereInstance);

      if (bAnimating)
      {
        static struct {
          int x = 1;
          int y = 1;
          int z = 1;
          int legs = 1;
          int wings = 1;
        } direction;
        // x direction
        if (thingPosition.x <= -boxSize/2) direction.x = +1;
        else if (thingPosition.x >= boxSize/2) direction.x = -1;
        thingPosition.x += 0.08*direction.x;
        // y direction
        if (thingPosition.y <= -boxSize/2) direction.y = +1;
        else if (thingPosition.y >= boxSize/2) direction.y = -1;
        thingPosition.y += 0.02*direction.y;
        // z direction
        if (thingPosition.z <= -boxSize/2) direction.z = +1;
        else if (thingPosition.z >= boxSize/2) direction.z = -1;
        thingPosition.z += 0.02*direction.z;
        // Legs angle
        if (thingLegsAngle <= 0) direction.legs = +1;
        else if (thingLegsAngle >= 90) direction.legs = -1;
        thingLegsAngle += 1.0*direction.legs;
        // Wings angle
        if (thingWingsAngle <= -30) direction.wings = +1;
        else if (thingWingsAngle >= 30) direction.wings = -1;
        thingWingsAngle += 1.0*direction.wings;

        thingRotate += 0.5;

        bVertexBuffersToUpdate = true;
      }

      glm::mat4 selectedWorldTransfo =
        glm::rotate(glm::radians(thingRotate), glm::vec3(0.0f, 0.0f, 1.0f)) *
        glm::translate(thingPosition);

      const glm::mat4& selectedModelTransfo =
        cubeInstance.bActive ? thingCubeConst :
        (teapotInstance.bActive ? thingTeapotConst : thingSphereConst);

      if (bModelChange)
      {
        updateCommandBuffers();
        bModelChange = false;
        bVertexBuffersToUpdate = true;
      }

      if (bVertexBuffersToUpdate)
      {
        checkModelVariables();
        std::cout << "The Thing is at (" << thingPosition.x << ", " << thingPosition.y << ", " << thingPosition.z << ")" << std::endl;

        thingLegsCommon =
          glm::translate( thingScale/2 *glm::vec3(sqrt(2.0f), 0.0f, -1.0f)) *
          glm::rotate(glm::radians(thingLegsAngle), glm::vec3(0.0f, 1.0f, 0.0f)) *
          glm::translate(glm::vec3(thingScale*legLength/2, 0.0f, 0.0f)) *
          glm::scale(glm::vec3(thingScale*legLength, legWidth, legWidth));
        thingWingsCommon =
          glm::rotate(glm::radians(thingWingsAngle), glm::vec3(0.0f, 1.0f, 0.0f)) *
          glm::translate(glm::vec3(thingScale/2 + wingLength/2, 0.0f, 0.0f)) *
          glm::scale(glm::vec3(wingLength, wingWidth, wingHeight));

        m_shader.updateInstance(
            selectedInstance,
            {thingColor},
            {selectedWorldTransfo * glm::scale(glm::vec3(thingScale)) * selectedModelTransfo}
            );
        m_shader.updateInstance(
            legsInstance,
            {thingLegsColor, thingLegsColor, thingLegsColor, thingLegsColor},
            {
              selectedWorldTransfo * thingLegsRot1 * thingLegsCommon,
              selectedWorldTransfo * thingLegsRot2 * thingLegsCommon,
              selectedWorldTransfo * thingLegsRot3 * thingLegsCommon,
              selectedWorldTransfo * thingLegsRot4 * thingLegsCommon
            }
            );
        m_shader.updateInstance(
            wingsInstance,
            {thingWingsColor, thingWingsColor},
            {
              selectedWorldTransfo * thingWingsRot1 * thingWingsCommon,
              selectedWorldTransfo * thingWingsRot2 * thingWingsCommon,
            }
            );
      }
    }*/
    m_shader.setUpdateBufferSwitch(bVertexBuffersToUpdate);
    m_shader.update();
    bVertexBuffersToUpdate = false;
  }

  void switchPolygonMode()
  {
    switch (polygonMode)
    {
      case VK_POLYGON_MODE_FILL:
        polygonMode = VK_POLYGON_MODE_LINE;
        break;
      case VK_POLYGON_MODE_LINE:
        polygonMode = VK_POLYGON_MODE_POINT;
        break;
      case VK_POLYGON_MODE_POINT:
        polygonMode = VK_POLYGON_MODE_FILL;
        break;
    }
  }

  // ### Controls ###
  virtual void processKeyPress(int key, int mods)
  {
      switch (key)
      {
        case GLFW_KEY_ESCAPE:
        case GLFW_KEY_Q:
          glfwSetWindowShouldClose(p_window, GLFW_TRUE);
          break;
        case GLFW_KEY_X:
          bShowAxes = !bShowAxes;
          m_shader.setShow(axesIndices[0], bShowAxes);
          m_shader.setShow(axesIndices[1], bShowAxes);
          m_shader.setShow(axesIndices[2], bShowAxes);
          bModelChange = true;
          break;
        case GLFW_KEY_V:
          m_shader.reload();
          recreateFromSwap();
          break;
        case GLFW_KEY_I:
          thetaCam = thetaInit;
          phiCam = phiInit;
          distCam = distInit;
          break;
        case GLFW_KEY_L:
          bLookAtMode = !bLookAtMode;
          break;
        case GLFW_KEY_G:
          switchPolygonMode();
          recreateFromSwap();
          break;
        case GLFW_KEY_M:
          switchModel();
          bModelChange = true;
          break;
        case GLFW_KEY_MINUS:
        case GLFW_KEY_KP_SUBTRACT:
          if ( distCam < 100.0 )
            distCam += 0.5;
          break;
        case GLFW_KEY_EQUAL: // Camera Control, go forward
          if (not (mods & (GLFW_MOD_SHIFT)))
            break;
        case GLFW_KEY_KP_ADD: // Camera Control, go backward
          if ( distCam > 1.0 )
            distCam -= 0.5;
          break;
        case GLFW_KEY_RIGHT: // Model Control, go right
          thingPosition.x += 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_LEFT: // Model Control, go left
          thingPosition.x -= 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_PAGE_UP: // Model Control, go up     2.0
          thingPosition.y += 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_PAGE_DOWN: // Model Control, go down
          thingPosition.y -= 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_DOWN: // Model Control, go backward
          thingPosition.z -= 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_UP: // Model Control, go forward
          thingPosition.z += 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_END: // Model Control, scale down
          thingScale -= 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_HOME: // Model Control, scale up
          thingScale += 0.1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_COMMA: // Model Control, rotate counter-clockwise
          thingRotate += 1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_PERIOD: // Model Control, rotate clockwise
          thingRotate -= 1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_LEFT_BRACKET: // Model Control, legs reduce angle
          thingLegsAngle += 1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_RIGHT_BRACKET: // Model Control, legs augment angle
          thingLegsAngle -= 1;
          bVertexBuffersToUpdate = true;
          break;
        case GLFW_KEY_SPACE:
          bAnimating = !bAnimating;
          break;
      }
  }

  virtual void processMouseClick(int button, int state, double x, double y)
  {
    switch (button)
    {
      case GLFW_MOUSE_BUTTON_LEFT:
        std::cout << "clicking at (" << x << ", " << y << ")" << std::endl;
        bMousePressed = (state == GLFW_PRESS);
        lastMouseX = x;
        lastMouseY = y;
    }
  }

  virtual void processMouseMotion(double x, double y)
  {
    if (bMousePressed)
    {
      int dx = x - lastMouseX;
      int dy = y - lastMouseY;

      thetaCam -= dx / 3.0;
      phiCam   -= dy / 3.0;

      lastMouseX = x;
      lastMouseY = y;
    }
  }
  virtual void processMouseWheel(double xoffset, double yoffset)
  {
    // TODO
  }

  //void generateHouseWorld()
  //{
  //  house = LoadModel(MODEL_PATH, m_shader.m_vertices, m_shader.m_indices);
  //  m_shader.add(glm::mat4(1.0f));
  //  m_shader.add(glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 2.0, 0.0)));

  //  // Set Projection matrix
  //  m_shader.mvp.model = glm::mat4(1.0f);
  //  m_shader.mvp.view = glm::mat4(1.0f);
  //  m_shader.mvp.view = glm::rotate(glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)), 0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
  //  m_shader.mvp.proj = glm::ortho(-12.0f, 12.0f, -8.0f, 8.0f, -10.0f, 10.0f);
  //}

  // #####################
  // Thing World functions
  // #####################
  void generateThingWorld()
  {
    thingScale = thingScaleInit;
    thingRotate = thingRotateInit;
    thingLegsAngle = thingLegsAngleInit;
    thingWingsAngle = thingWingsAngleInit;

    {
      auto axisXModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {1.0, 0.0, 0.0, 1.0}}, {0, 1});
      auto axisYModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {0.0, 1.0, 0.0, 1.0}}, {0, 1});
      auto axisZModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {0.0, 0.0, 1.0, 1.0}}, {0, 1});
      axesIndices[0] = m_shader.addDrawInstances(axisXModel, {{1.0, 0.0, 0.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
      axesIndices[1] = m_shader.addDrawInstances(axisYModel, {{0.0, 1.0, 0.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
  // #####################
      axesIndices[2] = m_shader.addDrawInstances(axisZModel, {{0.0, 0.0, 1.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
    }

    auto outerBoxModel = m_shader.addModel(cube_vertices, {0, 1, 2, 3, 4, 5, 6, 7, 0, 3, 1, 2, 4, 7, 5, 6, 0, 4, 1, 5, 2, 6, 3, 7, });
    m_shader.addDrawInstances(outerBoxModel, {{0.5, 0.0, 0.0, 0.0}}, {glm::scale(boxSize * glm::vec3(1.0f, 1.0f, 1.0f))}, PipelineManager::PIPELINE_TYPE::LINES);

    std::vector<uint32_t> cubeIndices = {0, 3, 1, 2, 5, 6, 4, 7, 1, 5, 0, 4, 3, 7, 2, 6,};
    auto cubeModel = m_shader.addModel(cube_vertices, cubeIndices, {{0, 8}, {8, 8}});
    cubeIdx = m_shader.addDrawInstances(cubeModel, {thingColor}, {thingCubeConst}, PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS);
    auto teapotModel = m_shader.addModel(gTeapotSommets, gTeapotConnec);
    teapotIdx = m_shader.addDrawInstances(teapotModel, {thingColor}, {thingTeapotConst});

    std::vector<glm::vec4> sphere_vertices;
    std::vector<uint32_t> sphere_indices;
    std::vector<DrawPart> pidxs;
    Shape::Sphere::make(8, 8, sphere_vertices, sphere_indices);
    Shape::Sphere::GetParts(8, 8, pidxs);
    auto sphereModel = m_shader.addModel(sphere_vertices, sphere_indices, pidxs);//, {{isn, icn}, {iss, ics}});
    sphereIdx = m_shader.addDrawInstances(sphereModel, {thingColor}, {thingSphereConst}, PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS);

    legsIdx = m_shader.addDrawInstances(
        cubeModel,
        {thingLegsColor, thingLegsColor, thingLegsColor, thingLegsColor},
        {thingLegsRot1 * thingLegsCommon, thingLegsRot2 * thingLegsCommon, thingLegsRot3 * thingLegsCommon, thingLegsRot4 * thingLegsCommon, },
        PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS
        );
    wingsIdx = m_shader.addDrawInstances(
        cubeModel,
        {thingWingsColor, thingWingsColor},
        {thingWingsRot1 * thingWingsCommon, thingWingsRot2 * thingWingsCommon},
        PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS
        );

    m_shader.setBuffers(vkCommandPool);
    // Set Projection matrix
    m_shader.vp.proj = glm::perspective(glm::radians(45.0f), m_swapchainMgmt.m_extent.width / (float) m_swapchainMgmt.m_extent.height, 0.1f, 100.0f);
    m_shader.vp.proj[1][1] *= -1;
  }

  // ############################
  // Solar System World functions
  // ############################
  void createPlanet(const float distance, const float size, glm::vec4 const& color, size_t& planetIdx, size_t& planetTorusIdx)
  {
    std::vector<glm::vec4> tVerts;
    std::vector<uint32_t> tIndis;
    std::vector<DrawPart> tParts;
#define ORBIT_TORUS_MNDIV 40
#define ORBIT_TORUS_PNDIV 1
#define ORBIT_TORUS_RADIUS 0.005
    PipelineManager::PIPELINE_TYPE torusPipeType = (ORBIT_TORUS_PNDIV > 1) ? PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS : PipelineManager::PIPELINE_TYPE::LINE_STRIPS;

    Shape::Torus::make(distance, ORBIT_TORUS_RADIUS, ORBIT_TORUS_MNDIV, ORBIT_TORUS_PNDIV, tVerts, tIndis);
    Shape::Torus::GetParts(ORBIT_TORUS_MNDIV, ORBIT_TORUS_PNDIV, tParts);
    auto torusModel = m_shader.addModel(tVerts, tIndis, tParts);
    planetTorusIdx = m_shader.addDrawInstances(torusModel, {color}, {glm::mat4(1.0)}, torusPipeType);
    planetIdx = m_shader.addDrawInstances(sphereModel, {color}, {glm::scale(glm::translate(glm::vec3(distance, 0.0, 0.0)), glm::vec3(size, size, size))}, PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS);
  };
  void generateSolarSystemWorld()
  {
    {
      auto axisXModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {1.0, 0.0, 0.0, 1.0}}, {0, 1});
      auto axisYModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {0.0, 1.0, 0.0, 1.0}}, {0, 1});
      auto axisZModel = m_shader.addModel({{0.0, 0.0, 0.0, 1.0}, {0.0, 0.0, 1.0, 1.0}}, {0, 1});
      axesIndices[0] = m_shader.addDrawInstances(axisXModel, {{1.0, 0.0, 0.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
      axesIndices[1] = m_shader.addDrawInstances(axisYModel, {{0.0, 1.0, 0.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
      axesIndices[2] = m_shader.addDrawInstances(axisZModel, {{0.0, 0.0, 1.0, 1.0}}, {glm::mat4(1.0)}, PipelineManager::PIPELINE_TYPE::LINES);
    }

#define NDIV 20
    std::vector<glm::vec4> tempVertices;
    std::vector<uint32_t> tempIndices;
    std::vector<DrawPart> pidxs;

    // Sphere model is used for every solar body
    Shape::Sphere::make(NDIV, NDIV, tempVertices, tempIndices);
    Shape::Sphere::GetParts(NDIV, NDIV, pidxs);
    sphereModel = m_shader.addModel(tempVertices, tempIndices, pidxs);//, {{isn, icn}, {iss, ics}});

    // Venus Instance
    createPlanet(mercuryDist, mercuryRad, mercuryColor, mercuryIdx, mercuryTorusIdx);
    createPlanet(venusDist, venusRad, venusColor, venusIdx, venusTorusIdx);
    createPlanet(earthDist, earthRad, earthColor, earthIdx, earthTorusIdx);
    createPlanet(marsDist, marsRad, marsColor, marsIdx, marsTorusIdx);
    createPlanet(jupiterDist, jupiterRad, jupiterColor, jupiterIdx, jupiterTorusIdx);
    createPlanet(saturnDist, saturnRad, saturnColor, saturnIdx, saturnTorusIdx);
    createPlanet(uranusDist, uranusRad, uranusColor, uranusIdx, uranusTorusIdx);
    createPlanet(neptuneDist, neptuneRad, neptuneColor, neptuneIdx, neptuneTorusIdx);

    {
      auto planeModel = m_shader.addModel(plane_vertices, plane_indices, {{0, 4}});
      m_shader.addDrawInstances(planeModel, {{0.2, 0.2, 0.2, 0.2}}, {glm::scale(glm::vec3(100.0, 100.0, 100.0))}, PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT);
    }

    // Sun instance
    // Should be drawn after everything else for transparency
    sunIdx = m_shader.addDrawInstances(sphereModel, {{1.0, 1.0, 0.0, 0.5}}, {glm::scale(glm::mat4(1.0), glm::vec3(1.8, 1.8, 1.8))}, PipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS_TRANSPARENT);

    m_shader.setBuffers(vkCommandPool);
    // Set Projection matrix
    m_shader.vp.proj = glm::perspective(glm::radians(45.0f), m_swapchainMgmt.m_extent.width / (float) m_swapchainMgmt.m_extent.height, 0.1f, 100.0f);
    m_shader.vp.proj[1][1] *= -1;
  }
};
