#pragma once

#include "Common.h"

#include <functional>
#include <vector>

#include "ShaderHelpers.h"

#include "DeviceManager.h"

class DescriptorManager
{
protected:
  VkDescriptorPool m_descriptorPool;

  DeviceManager const* m_pDeviceMgmt;

public:
  void init(DeviceManager const& deviceManager)
  {
    m_pDeviceMgmt = &deviceManager;
  }

  void createDescriptorPool(Shaders::ShaderPipeBase& shader)
  {
    auto const& layoutBindings = shader.GetLayoutBindings();

    std::vector<VkDescriptorPoolSize> poolSizes;
    poolSizes.reserve(layoutBindings.size());

    for (auto const& bind : layoutBindings)
    {
      VkDescriptorPoolSize poolSize = {};
      poolSize.type = bind.descriptorType;
      poolSize.descriptorCount = bind.descriptorCount;

      poolSizes.push_back(poolSize);
    }

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes = poolSizes.data();
    poolInfo.maxSets = 1;
    poolInfo.flags = 0; // Optional

    if (vkCreateDescriptorPool(m_pDeviceMgmt->getDevice(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create descriptor pool!");
    }
  }

  void createDescriptorSet(Shaders::ShaderPipeBase& shader)
  {
    VkDescriptorSetLayout layouts[] = {shader.m_descriptorSetLayout};
    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = m_descriptorPool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = layouts;

    if (vkAllocateDescriptorSets(m_pDeviceMgmt->getDevice(), &allocInfo, &shader.m_descriptorSet) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to allocate descriptor set");
    }
  }

  void updateDescriptorSet(Shaders::ShaderPipeBase& shader)
  {
    // For now, it is assumed there is only one of each descriptor type in the set.
    // Which is completely stupid, let's face it.
    auto& uniformInfos = shader.GetUniformInfos();
    auto& textureInfos = shader.GetTextureInfos();
    auto& bindings = shader.GetLayoutBindings();

    std::vector<VkWriteDescriptorSet> descriptorWrites;

    for (auto& bind : bindings)
    {
      VkWriteDescriptorSet writer = {};

      writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      writer.dstSet = shader.m_descriptorSet;
      writer.dstBinding = bind.binding;
      writer.dstArrayElement = 0;
      writer.descriptorType = bind.descriptorType;

      switch (writer.descriptorType)
      {
        // Uniforms
        case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
          {
            writer.descriptorCount = uniformInfos.size();
            writer.pBufferInfo = uniformInfos.data();
            break;
          }
        case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
          {
            writer.descriptorCount = static_cast<uint32_t>(textureInfos.size());
            writer.pImageInfo = textureInfos.data();
            break;
          }
        default:
          {
            writer.descriptorCount = 0;
          }
      }

      descriptorWrites.push_back(writer);
    }

    vkUpdateDescriptorSets(m_pDeviceMgmt->getDevice(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
  }

  void cleanup()
  {
    vkDestroyDescriptorPool(m_pDeviceMgmt->getDevice(), m_descriptorPool, nullptr);
  }
};
