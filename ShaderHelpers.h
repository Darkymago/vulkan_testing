#pragma once

#include <vector>
#include <fstream>

#include <map>
#include <vector>

#include "Common.h"

#include "FileLoader.h"
#include "ImageHelpers.h"

#include "DeviceManager.h"
#include "BufferManager.h"

typedef std::vector<VkVertexInputBindingDescription> BindingDescriptionVector;
typedef std::vector<VkVertexInputAttributeDescription> AttributeDescriptionVector;

typedef std::vector<VkPipelineShaderStageCreateInfo> ShaderCreateInfoVector;

class PipelineManager;

// Function taken from https://gist.github.com/graphitemaster/494f21190bb2c63c5516 for C++ offsetof on non-POD classe members
template<typename T1, typename T2>
inline size_t constexpr offset_of(T1 T2::*member)
{
  constexpr T2 object {};
  return size_t(&(object.*member)) - size_t(&object);
}

namespace Shaders
{
  struct ModulesManager
  {
    ShaderCreateInfoVector m_stagesInfos;

    void addModule(VkDevice device, std::string sFilename, VkShaderStageFlagBits stageFlag, std::string sEntryPoint = std::string("main"), bool bReplace = false)
    {
      if (!IsValidFlag(stageFlag))
      {
        throw std::runtime_error("invalid flag for shader module!");
      }

      VkPipelineShaderStageCreateInfo shaderStageInfo = {};
      shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
      shaderStageInfo.stage = stageFlag;
      shaderStageInfo.module = CreateModuleFromFile(device, sFilename);
      shaderStageInfo.pName = "main"; //sEntryPoint.c_str(); // TODO: manage other entry points

      m_stagesInfos.push_back(shaderStageInfo);
    }

    ShaderCreateInfoVector& getStagesInfos() { return m_stagesInfos; }

    void clear(VkDevice device)
    {
    // Cleanup
      for (auto& stageInfos : m_stagesInfos)
      {
        vkDestroyShaderModule(device, stageInfos.module, nullptr);
      }
      m_stagesInfos.clear();
    }

    static bool IsValidFlag(VkShaderStageFlagBits stageFlag)
    {
      switch (stageFlag)
      {
        case VK_SHADER_STAGE_ALL_GRAPHICS:
        case VK_SHADER_STAGE_ALL:
          return false;
        default:
          return true;
      }
    }

    static VkShaderModule CreateModule(VkDevice device, const std::vector<char>& code)
    {
      VkShaderModuleCreateInfo createInfo = {};
      createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
      createInfo.codeSize = code.size();
      createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

      VkShaderModule shaderModule;
      if( vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to create shader module!");
      }

      return shaderModule;
    }

    static VkShaderModule CreateModuleFromFile(VkDevice device, const std::string& filename)
    {
      auto shaderCode = Loaders::readFile(filename);
      return CreateModule(device, shaderCode);
    }
  };

  struct TextureDescriptors
  {
    uint32_t binding;
    std::vector<VkDescriptorImageInfo> textureInfos;

  public:
    void setBinding(const uint32_t binding) { this->binding = binding; }
    uint32_t getBinding() const { return binding; }

    void addDescriptor(VkImageView imageView, VkSampler sampler)
    {
      textureInfos.push_back( {sampler, imageView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL} );
    }
    void addDescriptors(std::vector<Images::ImageWrapper>& images, VkSampler sampler)
    {
      for (auto& image : images)
      {
        addDescriptor(image.imageView, sampler);
      }
      //auto prevEndIt = textureInfos.end();
      //textureInfos.insert(prevEndIt, images.size(), {sampler, 0, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL});

      //for (auto it = std::make_pair(prevEndIt, images.begin()); it.first != textureInfos.end(); ++it.first, ++it.second)
      //{
      //  (it.first)->imageView = (it.second)->imageView;
      //}
    }

    std::vector<VkDescriptorImageInfo>& getInfos() { return textureInfos; }
  };

  struct ShaderPipeBase
  {
    VkShaderStageFlags m_presentStages;
    VkDescriptorSetLayout m_descriptorSetLayout;
    VkDescriptorSet m_descriptorSet;

    ModulesManager m_modulesManager;

    DeviceManager const* m_pDeviceMgmt;
    BufferManager* m_pBufferMgmt;

  public:
    ~ShaderPipeBase() {};
    virtual void init(DeviceManager const& deviceManager, BufferManager& bufferManager)
    {
      m_pDeviceMgmt = &deviceManager;
      m_pBufferMgmt = &bufferManager;
    }
    virtual void cleanup()
    {
      vkDestroyDescriptorSetLayout(m_pDeviceMgmt->getDevice(), m_descriptorSetLayout, nullptr);
      m_modulesManager.clear(m_pDeviceMgmt->getDevice());
    }
    virtual void createDescriptorSetLayout()
    {
      auto& bindings = GetLayoutBindings();
      VkDescriptorSetLayoutCreateInfo layoutInfo = {};
      layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
      layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
      layoutInfo.pBindings = bindings.data();

      if (vkCreateDescriptorSetLayout(m_pDeviceMgmt->getDevice(), &layoutInfo, nullptr, &m_descriptorSetLayout) != VK_SUCCESS)
      {
        throw std::runtime_error("failed to create descriptor set layout!");
      }
    };

    virtual void draw(VkCommandBuffer, PipelineManager const&) = 0;
    virtual void update() = 0;

    void loadGenericShaders(std::string vertPath, std::string tessContPath, std::string tessEvalPath, std::string geomPath, std::string fragPath, std::string compPath, VkShaderStageFlags stageFlags)
    {
      m_presentStages = stageFlags;

      if (m_presentStages & VK_SHADER_STAGE_VERTEX_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), vertPath, VK_SHADER_STAGE_VERTEX_BIT);
      }
      if (m_presentStages & VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), tessContPath, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT);
      }
      if (m_presentStages & VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), tessEvalPath, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT);
      }
      if (m_presentStages & VK_SHADER_STAGE_GEOMETRY_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), geomPath, VK_SHADER_STAGE_GEOMETRY_BIT);
      }
      if (m_presentStages & VK_SHADER_STAGE_FRAGMENT_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), fragPath, VK_SHADER_STAGE_FRAGMENT_BIT);
      }
      if (m_presentStages & VK_SHADER_STAGE_COMPUTE_BIT)
      {
        m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), compPath, VK_SHADER_STAGE_COMPUTE_BIT);
      }
    }

    void loadMinimalGraphicsShaders(std::string vertPath, std::string fragPath)
    {
      m_presentStages = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
      m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), vertPath, VK_SHADER_STAGE_VERTEX_BIT);
      m_modulesManager.addModule(m_pDeviceMgmt->getDevice(), fragPath, VK_SHADER_STAGE_FRAGMENT_BIT);
    }

    virtual const std::vector<VkVertexInputAttributeDescription>& GetAttributes() = 0;
    virtual const std::vector<VkVertexInputBindingDescription>& GetBindings() = 0;
    virtual const std::vector<VkDescriptorSetLayoutBinding>& GetLayoutBindings() = 0;
    virtual std::vector<VkDescriptorBufferInfo>& GetUniformInfos() = 0;
    virtual std::vector<VkDescriptorImageInfo>& GetTextureInfos() = 0;

    virtual void setBuffers(VkCommandPool) = 0;
    virtual void cleanBuffers() = 0;
    virtual void setTextureView(VkImageView, VkSampler) = 0;
  };

  template<typename T> struct ShaderPipeAbstract : public ShaderPipeBase
  {
    ~ShaderPipeAbstract(){};
    const std::vector<VkVertexInputAttributeDescription>& GetAttributes()
    {
      return T::Attributes;
    }
    const std::vector<VkVertexInputBindingDescription>& GetBindings()
    {
      return T::Bindings;
    }
    const std::vector<VkDescriptorSetLayoutBinding>& GetLayoutBindings()
    {
      return T::LayoutBindings;
    }
    std::vector<VkDescriptorBufferInfo>& GetUniformInfos()
    {
      return T::UniformInfos;
    }
    std::vector<VkDescriptorImageInfo>& GetTextureInfos()
    {
      return T::TextureInfos;
    }

    virtual void setUniformBuffer(BufferHandle& handle)
    {
      VkBuffer buffer = m_pBufferMgmt->getBuffer(handle);
      for (auto& info : T::UniformInfos)
      {
        info.buffer = buffer;
      }
    }
    void setTextureView(VkImageView imageView, VkSampler sampler)
    {
      for (auto& info : T::TextureInfos)
      {
        info.sampler = sampler;
        info.imageView = imageView;
      }
    }
  };
}
