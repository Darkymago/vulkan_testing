#include <vector>

#include <glm/glm.hpp>

namespace Shape
{
  namespace Torus
  {
    void make(const float R, const float r, const uint32_t nMer, const uint32_t nPar, std::vector<glm::vec4>& vertices, std::vector<uint32_t>& indices)
    {
      vertices.clear();
      indices.clear();

      const float rt = (nPar>1) ? r : 0;
      const float meridianAngleUnit = M_PI/nMer;
      const float lattUnit = M_PI/nPar;
      float longRad, lattRad;

      // Vertices
      for (int iMer = 0; iMer < nMer; ++iMer)
      {
        float longitude = 360.0f*iMer/nMer;

        for  (int iPar = 0; iPar < nPar; ++iPar)
        {
          float lattitude = 360.0f * iPar / nPar;

          longRad = glm::radians(longitude);
          lattRad = glm::radians(lattitude);

          vertices.push_back({
              (R + rt * cos(lattRad)) * cos(longRad),
              (R + rt * cos(lattRad)) * sin(longRad),
              rt * sin(lattRad),
              1.0f
              });
        }
      }

      // Indices
      if (nPar > 1)
        for (int im = 0; im < nMer; ++im)
        {
          for (int ip = 0; ip < nPar; ++ip)
          {
            indices.push_back(im * nPar + ip);
            indices.push_back(((im+1) % nMer) * nPar + ip);
          }
          indices.push_back(im * nPar);
          indices.push_back(((im+1) % nMer) * nPar);
        }
      else
        for (int im = 0; im < nMer+1; ++im)
          indices.push_back(im % nMer);
    }

    void GetParts(uint32_t nMer, uint32_t nPar, std::vector<std::array<uint32_t, 2>>& pidxs)
    {
      pidxs.clear();
      if (nPar > 1)
        for (int im = 0; im < nMer; ++im)
          pidxs.push_back( {(2*nPar + 2) * im, (2*nPar+2)} );
      else
        pidxs.push_back( {0, nMer+1} );

    }
  }

  namespace Sphere
  {
    void make(const uint32_t nMer, const uint32_t nPar, std::vector<glm::vec4>& vertices, std::vector<uint32_t>& indices)
    {
      // make sure vertices and indices vectors are clear
      vertices.clear();
      indices.clear();

      const float meridianAngleUnit = M_PI/nMer;
      const float lattUnit = M_PI/nPar;
      float longRad, lattRad;

      // Vertices
      // North pole
      vertices.push_back({0.0f, 0.0f, 0.5f, 1.0f});
      // South pole
      vertices.push_back({0.0f, 0.0f, -0.5f, 1.0f});
      // Meridians section
      for (int iMer = 0; iMer < nMer; ++iMer)
      {
        float longitude = 360.0f*iMer/nMer;

        for (int iPar = 0; iPar < nPar; ++iPar)
        {
          float lattitude = 180.0f*(iPar+1)/(nPar+1);

          longRad = glm::radians(longitude);
          lattRad = glm::radians(lattitude);

          vertices.push_back({
              0.5f*sin(lattRad)*cos(longRad),
              0.5f*sin(lattRad)*sin(longRad),
              0.5f*cos(lattRad),
              1.0f
              });
        }
      }

      // Indices
      for (int im = 0; im < nMer; ++im)
      {
        indices.push_back(0);
        for (int ip = 0; ip < nPar; ++ip)
        {
          indices.push_back(2 + (im) * nPar + ip);
          indices.push_back(2 + ((im+1) % nMer) * nPar + ip);
        }
        indices.push_back(1);
      }
    }

    void GetParts(uint32_t nMer, uint32_t nPar, std::vector<std::array<uint32_t, 2>>& pidxs)
    {
      pidxs.clear();
      for (int im = 0; im < nMer; ++im)
        pidxs.push_back({((2*nPar+2)*(im)), 2*nPar+2});
    }
  }

}
