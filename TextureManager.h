#pragma once

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

#include <string>
#include <vector>
#include <stdexcept>

#include <iostream>

#include "BufferHelpers.h"
#include "ImageHelpers.h"

#include "DeviceManager.h"

class TextureManager
{
public:
  VkSampler m_textureSampler = VK_NULL_HANDLE;

  std::vector<Images::ImageWrapper> m_textures;

  DeviceManager const* m_pDeviceMgmt;

public:
  void init(DeviceManager const& deviceManager)
  {
    m_pDeviceMgmt = &deviceManager;
  }

  void addTexture(const std::string& sPath)
  {
    Images::ImageWrapper image;
    image.load(sPath);

    if (!image.pixels)
    {
      throw std::runtime_error("failed te load texture image!");
    }

    if (!m_textures.empty())
    {
      Images::ImageWrapper& prevImage = m_textures[m_textures.size()-1];
      image.offset = prevImage.offset + prevImage.size;
    }

    m_textures.push_back(image);
  }

  void sendTexturesToDevice(VkCommandPool commandPool)
  {
    VkDeviceSize totalSize = 0;

    for (auto& image : m_textures)
    {
      totalSize += image.size;
    }


    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    Buffers::CreateBuffer(
        m_pDeviceMgmt->getDeviceSet(),
        totalSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        stagingBuffer,
        stagingBufferMemory
    );

    void* data;
    for (auto& image : m_textures)
    {
      vkMapMemory(m_pDeviceMgmt->getDevice(), stagingBufferMemory, image.offset, image.size, 0, &data);
      {
        memcpy(data, image.pixels, static_cast<size_t>(image.size));
      }
      vkUnmapMemory(m_pDeviceMgmt->getDevice(), stagingBufferMemory);

      stbi_image_free(image.pixels);

      Images::CreateImage(
          m_pDeviceMgmt->getDeviceSet(),
          image.width,
          image.height,
          VK_FORMAT_R8G8B8A8_UNORM,
          VK_IMAGE_TILING_OPTIMAL,
          VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
          image.image,
          image.imageMemory
      );

      Images::TransitionImageLayout(m_pDeviceMgmt->getDevice(), commandPool, m_pDeviceMgmt->getCommandQueue(), image.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
      Images::CopyBufferToImage(m_pDeviceMgmt->getDevice(), commandPool, m_pDeviceMgmt->getCommandQueue(), stagingBuffer, image.image, static_cast<uint32_t>(image.width), static_cast<uint32_t>(image.height));
      Images::TransitionImageLayout(m_pDeviceMgmt->getDevice(), commandPool, m_pDeviceMgmt->getCommandQueue(), image.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }

    vkDestroyBuffer(m_pDeviceMgmt->getDevice(), stagingBuffer, nullptr);
    vkFreeMemory(m_pDeviceMgmt->getDevice(), stagingBufferMemory, nullptr);
  }

  void createTextureViews()
  {
    for (auto& image : m_textures)
    {
      image.imageView = Images::CreateImageView(m_pDeviceMgmt->getDeviceSet(), image.image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
    }
  }

  void createTextureSampler()
  {
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;

    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;

    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;

    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;

    samplerInfo.unnormalizedCoordinates = VK_FALSE;

    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;

    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;

    if (vkCreateSampler(m_pDeviceMgmt->getDevice(), &samplerInfo, nullptr, &m_textureSampler) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create texture sampler!");
    }
  }

  void cleanup()
  {
    vkDestroySampler(m_pDeviceMgmt->getDevice(), m_textureSampler, nullptr);

    for (auto& image : m_textures)
    {
      vkDestroyImageView(m_pDeviceMgmt->getDevice(), image.imageView, nullptr);

      vkDestroyImage(m_pDeviceMgmt->getDevice(), image.image, nullptr);
      vkFreeMemory(m_pDeviceMgmt->getDevice(), image.imageMemory, nullptr);
    }
  }
};
