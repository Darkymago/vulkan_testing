#pragma once

#include "VulkanShaderHelper.h"
#include "VulkanObjLoader.h"

#include "VulkanPipelineManager.h"

#include <glm/glm.hpp>

//struct Vertex
//{
//  glm::vec4 pos;
//  glm::vec4 color;
//};

//{    x,    y,    z,   r,   g,   b}
std::vector<glm::vec4> lines_vertices = {
  {-12.0,  0.0, 0.0, 1.0 },
  { 12.0,  0.0, 0.0, 1.0 },
  {- 4.0, -8.0, 0.0, 1.0 },
  {- 4.0,  8.0, 0.0, 1.0 },
  {  4.0, -8.0, 0.0, 1.0 },
  {  4.0,  8.0, 0.0, 1.0 }
};
std::vector<glm::vec4> w_vertices = {
  { -4.0,  2.0, 0.0, 1.0 },
  { -3.0, -3.0, 0.0, 1.0 },
  { -1.0, -3.0, 0.0, 1.0 },
  {  0.0,  0.0, 0.0, 1.0 },
  {  1.0, -3.0, 0.0, 1.0 },
  {  3.0, -3.0, 0.0, 1.0 },
  {  4.0,  2.0, 0.0, 1.0 }
};
std::vector<glm::vec4> arrow_vertices = {
  { -3.0,  1.0, 0.0, 1.0 },
  { -3.0, -1.0, 0.0, 1.0 },
  {  0.0, -1.0, 0.0, 1.0 },
  { -0.5, -2.5, 0.0, 1.0 },
  {  3.0,  0.0, 0.0, 1.0 },
  { -0.5,  2.5, 0.0, 1.0 },
  {  0.0,  1.0, 0.0, 1.0 }
};

std::vector<glm::vec4> colors = {
  { 1.0, 1.0, 1.0, 1.0 }, // White
  { 1.0, 0.0, 0.0, 1.0 }, // Red
  { 0.0, 1.0, 0.0, 1.0 }, // Green
  { 0.0, 0.0, 1.0, 1.0 }, // Blue
  { 0.0, 1.0, 1.0, 1.0 }, // Cyan
  { 1.0, 0.0, 1.0, 1.0 }, // Magenta
  { 1.0, 1.0, 0.0, 1.0 }  // Yellow
};

std::vector<glm::vec4> instance_translation = {
  { 0.0, 0.0, 0.0, 0.0 }, // Lines
  {-8.0, 4.0, 0.0, 0.0 }, // Red Upper Left Corner
  { 0.0, 4.0, 0.0, 0.0 }, // Green Upper Center
  { 8.0, 4.0, 0.0, 0.0 }, // Blue Upper Right Corner
  {-8.0,-4.0, 0.0, 0.0 }, // Cyan Lower Left Corner
  { 0.0,-4.0, 0.0, 0.0 }, // Magenta Lower Center
  { 8.0,-4.0, 0.0, 0.0 }  // Yellow Lower Right Corner
};

struct PrimitiveShader : public Shaders::ShaderPipeAbstract<PrimitiveShader>
{
  const std::string VERTEX_SHADER_PATH = "shaders/primitive.vert.spv";
  const std::string FRAGMENT_SHADER_PATH = "shaders/primitive.frag.spv";

  static std::vector<VkVertexInputAttributeDescription> const Attributes;
  static std::vector<VkVertexInputBindingDescription> const Bindings;
  static std::vector<VkDescriptorSetLayoutBinding> const LayoutBindings;

  static std::vector<VkDescriptorBufferInfo> UniformInfos;
  static std::vector<VkDescriptorImageInfo> TextureInfos;

  std::vector<glm::vec4> m_vertices;
  std::vector<uint32_t> m_vert_indices;

  std::vector<glm::vec4> m_colors;
  std::vector<glm::vec4> m_translations;

  Objects::MVPMatrices mvp;

  bool bForms = false;

  struct
  {
    BufferHandle uniform;
    BufferHandle vertices;
    BufferHandle vert_indices;
    BufferHandle colors;
    BufferHandle translations;
  } buffers;

  void init(VulkanDeviceManager const& deviceManager, VulkanBufferManager& bufferManager)
  {
    ShaderPipeAbstract::init(deviceManager, bufferManager);

    loadMinimalGraphicsShaders(VERTEX_SHADER_PATH, FRAGMENT_SHADER_PATH);
    createDescriptorSetLayout();

    // Init Vertices
    m_vertices.reserve(lines_vertices.size() + w_vertices.size() + arrow_vertices.size());
    m_vertices.insert(m_vertices.end(), lines_vertices.begin(), lines_vertices.end());
    m_vertices.insert(m_vertices.end(), w_vertices.begin(), w_vertices.end());
    m_vertices.insert(m_vertices.end(), arrow_vertices.begin(), arrow_vertices.end());

    // Init Indices
    m_vert_indices = {
      // --- Lines --- 0-5
      0, 1,
      2, 3,
      4, 5,
      // --- W shapes ---
      // Triangles - 6-20
      6, 7, 9,
      8, 9, 7,
      6, 9, 12,
      12, 9, 11,
      11, 9, 10,
      // Triangle Strip - 21-27
      7, 8, 6, 9, 12, 10, 11,
      // Tiangle Fan - 28-34
      9, 10, 11, 12, 6, 7, 8,
      // --- Arrow shapes ---
      // Triangles - 35-49
      13, 14, 15,
      15, 19, 13,
      18, 19, 17,
      19, 15, 17,
      15, 16, 17,
      // Triangle Strips - 50-59
      13, 14, 19, 15, 17, 16, 17, 17, 19, 18,
      // Triangle Fan - 60-66
      17, 18, 19, 13, 14, 15, 16
    };

    // Init Colors
    m_colors = colors;

    m_translations = instance_translation;
  }

  void set_bForms(bool b) {bForms = b;}
  void draw(VkCommandBuffer commandBuffer, VulkanPipelineManager const& pipelineMgmt)
  {
    VkBuffer vertexBuffers[] = {
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.vertices),
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.colors),
      ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.translations)
    };
    VkDeviceSize buffer_offsets[] = {0, 0, 0};

    VkDeviceSize indices_offsets[] = {0, 6, 21, 28, 35, 50, 60};
    VkDeviceSize instances_offsets[] = {0, 1, 4, 6};

    // Constant Bindings
    vkCmdBindVertexBuffers(commandBuffer, 0, 3, vertexBuffers, buffer_offsets);
    vkCmdBindIndexBuffer(commandBuffer, ShaderPipeBase::m_pBufferMgmt->getBuffer(buffers.vert_indices), indices_offsets[0], VK_INDEX_TYPE_UINT32);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.m_pipelineLayout, 0, 1, &static_cast<ShaderPipeBase*>(this)->m_descriptorSet, 0, nullptr);

    // Drawing Lines
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getPipeline(VulkanPipelineManager::PIPELINE_TYPE::LINES));
    vkCmdDrawIndexed(commandBuffer, 6, 1, indices_offsets[0], 0, instances_offsets[0]);

    // Drawing with Triangles
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getPipeline(VulkanPipelineManager::PIPELINE_TYPE::TRIANGLES));
    if (bForms)
    {
      vkCmdDrawIndexed(commandBuffer, 15, 3, indices_offsets[4], 0, instances_offsets[1]);
    } else
    {
      vkCmdDrawIndexed(commandBuffer, 15, 3, indices_offsets[1], 0, instances_offsets[1]);
    }

    // Drawing with Triangles Strips
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getPipeline(VulkanPipelineManager::PIPELINE_TYPE::TRIANGLE_STRIPS));
    if (bForms)
    {
      vkCmdDrawIndexed(commandBuffer, 10, 2, indices_offsets[5], 0, instances_offsets[2]);
    } else
    {
      vkCmdDrawIndexed(commandBuffer, 7, 2, indices_offsets[2], 0, instances_offsets[2]);
    }

    // Drawing with Triangles Fans
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineMgmt.getPipeline(VulkanPipelineManager::PIPELINE_TYPE::TRIANGLE_FANS));
    if (bForms)
    {
      vkCmdDrawIndexed(commandBuffer, 7, 1, indices_offsets[6], 0, instances_offsets[3]);
    } else
    {
      vkCmdDrawIndexed(commandBuffer, 7, 1, indices_offsets[3], 0, instances_offsets[3]);
    }
  }
  void update()
  {
    ShaderPipeBase::m_pBufferMgmt->updateBuffer(buffers.uniform, sizeof(Objects::MVPMatrices), &mvp);
  }

  void setBuffers(VkCommandPool commandPool)
  {
    // Vertex Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_vertices[0])*m_vertices.size(),
        (void*) m_vertices.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.vertices
    );
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_vert_indices[0])*m_vert_indices.size(),
        (void*) m_vert_indices.data(),
        commandPool,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        buffers.vert_indices
    );
    // Color Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_colors[0])*m_colors.size(),
        (void*) m_colors.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.colors
    );
    // Translations Buffers
    ShaderPipeBase::m_pBufferMgmt->createBuffer(
        sizeof(m_translations[0])*m_translations.size(),
        (void*) m_translations.data(),
        commandPool,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        buffers.translations
    );
  }
  void setUniformHandle(BufferHandle& handle)
  {
    buffers.uniform = handle;
  }
  void cleanBuffers()
  {
    m_pBufferMgmt->cleanBuffer(buffers.vertices);
    m_pBufferMgmt->cleanBuffer(buffers.vert_indices);
    m_pBufferMgmt->cleanBuffer(buffers.colors);
    m_pBufferMgmt->cleanBuffer(buffers.translations);
    m_pBufferMgmt->cleanBuffer(buffers.uniform);
  }
  void clear(VkDevice device)
  {
    m_vertices.clear();
    m_vert_indices.clear();
    m_colors.clear();
    m_translations.clear();
  }
};
const std::vector<VkVertexInputAttributeDescription> PrimitiveShader::Attributes = {
  // { location, binding, VkFormat, offset }
  {0, 0, VK_FORMAT_R32G32B32A32_SFLOAT, 0}, // Vertex
  {1, 1, VK_FORMAT_R32G32B32A32_SFLOAT, 0}, // Color
  {2, 2, VK_FORMAT_R32G32B32A32_SFLOAT, 0}  // Translation
};
const std::vector<VkVertexInputBindingDescription> PrimitiveShader::Bindings = {
  // { binding, size, VkInputRate }
  {0, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_VERTEX},   // Vertex
  {1, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_INSTANCE}, // Color
  {2, sizeof(glm::vec4), VK_VERTEX_INPUT_RATE_INSTANCE}  // Translation
};
const std::vector<VkDescriptorSetLayoutBinding> PrimitiveShader::LayoutBindings = {
  // { binding, VkDescriptorType, count, VkShaderStageFlags, pImmutableSamplers }
  {0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT, nullptr}
};
std::vector<VkDescriptorBufferInfo> PrimitiveShader::UniformInfos = {
  // { VkHandle (Buffer to set after creation), offset, size }
  {VK_NULL_HANDLE, 0, sizeof(Objects::MVPMatrices)},
};
std::vector<VkDescriptorImageInfo> PrimitiveShader::TextureInfos = {
  // { VkHandle (Buffer to set after creation), offset, size }
};
